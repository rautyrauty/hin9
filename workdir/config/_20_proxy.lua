
--server_remote = "http://localhost:8080/" -- this is needed, set it either defines or here

do_proxy = create_round_robin (server_remote, server_remote)

if (do_proxy == nil) then return end

local location = "/proxy" -- proxy only things inside this folder
local format = string.format ("^%s(/.*)", location)

map (nil, location .. "/*", const.MAP_PREFILE, function (req)
  local path, query, method, version, host = parse_path (req)
  local sub_path = string.match (path, format)
  local url = sub_path or "/"
  if (query) then
    url = string.format ("%s?%s", url, query)
  end
  return do_proxy (req, url)
end)



