
-- logging
map (nil, "*", const.MAP_FINISH, function (req)
  access (req, "%I - - [%D] \"%M %Q %V\" %S %Z\n")
end)

-- default map
map (nil, "*", const.MAP_PREFILE, function (req)
  local path = req_get_path (req)

  local dir_path, file_name, ext, path_info, location = set_path (req, path, index_files)
  if (location) then
    return redirect (req, location, 301)
  end
  if (file_name == nil) then
    if (dir_path) then
      return list_dir (req)
    else
      return respond (req, 404)
    end
  end

  if (forbidden_files[ext]) then
    return respond (req, 403)
  end

  if (to_compress[ext] == nil) then
    req_set_disabled (req, "compress")
  end
  if (content_type[ext]) then
    set_content_type (req, content_type[ext])
  end

  if (fpm_socks[ext]) then
    return fastcgi (req, fpm_socks[ext])
  elseif (to_cache[ext]) then
    req_set_cache (req, 604800)
  end

  send_file (req)
  return true
end)

