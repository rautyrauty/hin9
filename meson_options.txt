option('fcgi', type : 'boolean', value : true,
 description: 'compile fcgi support')

option('openssl', type : 'boolean', value : true,
 description: 'use openssl')

option('cgi', type : 'boolean', value : false,
 description: 'compile cgi support')

option('rproxy', type : 'boolean', value : true,
 description: 'compile reverse proxy support')

option('ffcall', type : 'boolean', value : false,
 description: 'use ffcall library to enable more format specifier for lua log')

option('force-lua-version', type : 'string',
 description: 'request a specific lua version')
