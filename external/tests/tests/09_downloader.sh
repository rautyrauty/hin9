
set -e

export HOST_URL="http://$BENCH_HOST:$BENCH_PORT/"

export module="home"
export file_url=""
export file_path="index.html"
sh $TOOL_DIR/downloader_test.sh

export module="large"
export file_url="bin/$TEST_FILE_TEXT"
export file_path="bin/$TEST_FILE_TEXT"
sh $TOOL_DIR/downloader_test.sh

echo "Completed test $name all"

