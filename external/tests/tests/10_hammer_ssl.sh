
set -e

URL=https://$BENCH_HOST:$BENCH_PORTS/

RET="$(ab -k -c $BENCH_CON -n $BENCH_NUM $URL)"
RET_CODE=$?

export RET
export RET_CODE

sh $TOOL_DIR/hammer.sh
