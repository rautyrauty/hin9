
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "conf.h"

// used to track errors and finished state
static int state_callback (http_client_t * http, uint32_t state, uintptr_t data) {
  switch (state) {
  case HIN_HTTP_STATE_SSL_FAILED: // fall-through
  case HIN_HTTP_STATE_CONNECTION_FAILED:
  case HIN_HTTP_STATE_HEADERS_FAILED:
  case HIN_HTTP_STATE_ERROR:
  case HIN_HTTP_STATE_FINISH:
    fprintf (stderr, "done.\n");
    hin_stop ();
  break;
  default:
  break;
  }
  return 0;
}

static int read_callback (hin_pipe_t * pipe, hin_buffer_t * buf, int num, int flush) {
  if (num <= 0) return 1;
  buf->count = num;
  hin_pipe_append_raw (pipe, buf);

  http_client_t * http = pipe->parent;
  if (num == 0 && flush != 0 && http->sz) {
  }

  if (http->debug & HNDBG_PROGRESS) {
    //download_progress (http, pipe, num, flush);
  }
  //if (flush) return 1; // already cleaned in the write done handler
  return 0;
}

http_client_t * http_download_raw (const char * url1, const char * output) {
  // get a connection
  http_client_t * http = http_connection_get (url1);

  http->debug = hin_g.debug;

  // should be closed after download finishes
  FILE * fp = fopen (output, "w");

  http->read_callback = read_callback;
  http->state_callback = state_callback;
  // if a fd is given it outputs to fd else outputs to console
  http->save_fd = fileno (fp);

  http_connection_start (http);

  return http;
}

/*
  download an url to a file
*/
int main (int argc, const char * argv[]) {
  // initialize basic data
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = 0;

  hin_init ();

  // start a download object
  http_download_raw ("https://tiotags.gitlab.io/", "output.txt");

  // process all events and exit when hin_stop is issued
  hin_event_loop ();

  hin_clean ();
  return 0;
}



