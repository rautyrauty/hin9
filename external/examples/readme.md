
hin9 examples
=============

These are example programs for the hin.so http library.

compile & run
-------------

compile the library

`mkdir -p build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make`

compile examples

`cmake ../external/examples/ && make`

run examples e.g.

`./download`
