
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"
//#include "conf.h"

typedef struct {
  hin_pipe_t * pipe;
  time_t last;
  off_t last_sz;
} hin_download_tracker_t;

static int to_human_bytes (off_t amount, double * out, char ** unit) {
  *unit = "B";
  *out = amount;
  if (amount < 1024) goto end;
  *unit = "KB";
  *out = amount / 1024.0;
  amount /= 1024;
  if (amount < 1024) goto end;
  *unit = "MB";
  *out = amount / 1024.0;
  amount /= 1024;
  if (amount < 1024) goto end;
  *unit = "GB";
  *out = amount / 1024.0;
  amount /= 1024;
  if (amount < 1024) goto end;
  *unit = "TB";
end:
  return 0;
}

static int download_progress (http_client_t * http, hin_pipe_t * pipe, int num, int flush) {
  hin_download_tracker_t * p = http->progress;
  if (p == NULL) {
    p = calloc (1, sizeof (*p));
    p->pipe = pipe;
    http->progress = p;
  }

  p->last_sz += num;
  if ((time (NULL) == p->last) && (flush == 0)) return 0;
  int single = 0;
  p->last = time (NULL);
  if (single) {
    printf ("\r");
  }
  char * u1, * u2, * u3;
  double s1, s2, s3;
  to_human_bytes (pipe->out.pos + num, &s1, &u1);
  to_human_bytes (http->sz, &s2, &u2);
  to_human_bytes (p->last_sz, &s3, &u3);

  fprintf (stderr, "%.*s: %.1f%s/%.1f%s \t%.1f %s/sec%s",
	(int)http->uri.all.len, http->uri.all.ptr,
	s1, u1,
	s2, u2,
	s3, u3,
	flush ? " finished" : "");
  p->last_sz = 0;
  if (!single || flush) {
    printf ("\n");
  }
  if (flush) {
    free (p);
    http->progress = NULL;
  }
  return 0;
}

#include <unistd.h>
#include <fcntl.h>

static int http_autoname (http_client_t * http) {
  char * path = NULL;
  if (path == NULL) {
    char * old_path = strndup (http->uri.path.ptr, http->uri.path.len);
    char * fn = strrchr (old_path, '/');
    fn++;
    if (*fn != '\0') path = strdup (fn);
    free (old_path);
  }
  if (path == NULL) path = strdup ("noname");
  http->save_fd = open (path, O_RDWR | O_CLOEXEC | O_TRUNC | O_CREAT, 0666);
  if (http->save_fd < 0) {
    printf ("error! can't open %s %s\n", path, strerror (errno));
    free (path);
    return -1;
  }
  printf ("downloading '%s' to '%s'\n", http->uri.all.ptr, path);
  free (path);
  return 0;
}

static int state_callback (http_client_t * http, uint32_t state, uintptr_t data) {
  if (state == HIN_HTTP_STATE_HEADERS) {
    if (http->flags & HIN_FLAG_AUTONAME) {
      http_autoname (http);
    }
  }
  return 0;
}

static int read_callback (hin_pipe_t * pipe, hin_buffer_t * buf, int num, int flush) {
  if (num <= 0) return 1;
  buf->count = num;
  hin_pipe_append_raw (pipe, buf);

  http_client_t * http = pipe->parent;
  if (num == 0 && flush != 0 && http->sz) {
  }

  if (http->debug & HNDBG_PROGRESS) {
    download_progress (http, pipe, num, flush);
  }
  if (flush) hin_stop ();
  //if (flush) return 1; // already cleaned in the write done handler
  return 0;
}

http_client_t * http_download_progress (const char * url1, const char * out_path) {
  http_client_t * http = http_connection_get (url1);
  http->debug = hin_g.debug | HNDBG_PROGRESS;

  http->read_callback = read_callback;
  http->state_callback = state_callback;

  if (out_path == NULL) {
    http->flags |= HIN_FLAG_AUTONAME;
  } else {
    FILE * fp = fopen (out_path, "w");
    http->save_fd = fileno (fp);
  }

  http_connection_start (http);

  return http;
}

/*
  download an url to a file but show progress on the std out
*/
int main (int argc, const char * argv[]) {
  // initialize basic data
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = 0;

  hin_init ();

  // parse arguments
  if (argc < 1 || argc > 2) {
    printf ("usage %s <url> <save path>\n", argv[0]);
    exit (1);
  }

  // a better url is required to show the progress
  const char * url = "https://tiotags.gitlab.io/";
  const char * out_path = NULL;

  if (argc > 1) url = argv[1];
  if (argc > 2) out_path = argv[2];

  // start a download object
  http_download_progress (url, out_path);

  // process all events and exit when hin_stop is issued
  hin_event_loop ();

  hin_clean ();
  return 0;
}

