
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"

int main (int argc, char * argv[]) {
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = HNDBG_BASIC;

  hin_init ();

  httpd_create (NULL, "8080", NULL, NULL);

  hin_event_loop ();

  hin_clean ();
  return 0;
}


