/*
 * basic_libs, libraries used for other projects, including, pattern matching, timers and others
 * written by Alexandru C
 * You may not use this software except in compliance with the License.
 * You may obtain a copy of the License at: docs/LICENSE.txt
 * documentation is in the docs folder
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <stdarg.h>

#include "basic_pattern.h"
#include "basic_types.h"

// TODO patterns only handles ascii atm

enum {RUN_ONE = 0x0, RUN_ALL=0x10, RUN_ZERO=0x1};

static uint8_t dmatch[256] = {
PATTERN_CONTROL,				//	00	NUL
PATTERN_CONTROL,				//	01	SOH
PATTERN_CONTROL,				//	02	STX
PATTERN_CONTROL,				//	03	ETX
PATTERN_CONTROL,				//	04	EOT
PATTERN_CONTROL,				//	05	ENQ
PATTERN_CONTROL,				//	06	ACK
PATTERN_CONTROL,				//	07	BEL
PATTERN_CONTROL,				//	08	BS
PATTERN_CONTROL|PATTERN_SPACE,			//	09	HT
PATTERN_CONTROL|PATTERN_SPACE,			//	0A	LF
PATTERN_CONTROL|PATTERN_SPACE,			//	0B	VT
PATTERN_CONTROL|PATTERN_SPACE,			//	0C	FF
PATTERN_CONTROL|PATTERN_SPACE,			//	0D	CR
PATTERN_CONTROL,				//	0E	SO
PATTERN_CONTROL,				//	0F	SI
PATTERN_CONTROL,				//	10	DLE
PATTERN_CONTROL,				//	11	DC1
PATTERN_CONTROL,				//	12	DC2
PATTERN_CONTROL,				//	13	DC3
PATTERN_CONTROL,				//	14	DC4
PATTERN_CONTROL,				//	15	NAK
PATTERN_CONTROL,				//	16	SYN
PATTERN_CONTROL,				//	17	ETB
PATTERN_CONTROL,				//	18	CAN
PATTERN_CONTROL,				//	19	EM
PATTERN_CONTROL,				//	1A	SUB
PATTERN_CONTROL,				//	1B	ESC
PATTERN_CONTROL,				//	1C	FS
PATTERN_CONTROL,				//	1D	GS
PATTERN_CONTROL,				//	1E	RS
PATTERN_CONTROL,				//	1F	US
PATTERN_SPACE,					//	20	space
PATTERN_PUNCTUATION,				//	21	!
PATTERN_PUNCTUATION,				//	22	"
PATTERN_PUNCTUATION,				//	23	#
PATTERN_PUNCTUATION,				//	24	$
PATTERN_PUNCTUATION,				//	25	%
PATTERN_PUNCTUATION,				//	26	&
PATTERN_PUNCTUATION,				//	27	'
PATTERN_PUNCTUATION,				//	28	(
PATTERN_PUNCTUATION,				//	29	)
PATTERN_PUNCTUATION,				//	2A	*
PATTERN_PUNCTUATION,				//	2B	+
PATTERN_PUNCTUATION,				//	2C	,
PATTERN_PUNCTUATION,				//	2D	-
PATTERN_PUNCTUATION,				//	2E	.
PATTERN_PUNCTUATION,				//	2F	/
PATTERN_DIGIT|PATTERN_HEXA,			//	30	0
PATTERN_DIGIT|PATTERN_HEXA,			//	31	1
PATTERN_DIGIT|PATTERN_HEXA,			//	32	2
PATTERN_DIGIT|PATTERN_HEXA,			//	33	3
PATTERN_DIGIT|PATTERN_HEXA,			//	34	4
PATTERN_DIGIT|PATTERN_HEXA,			//	35	5
PATTERN_DIGIT|PATTERN_HEXA,			//	36	6
PATTERN_DIGIT|PATTERN_HEXA,			//	37	7
PATTERN_DIGIT|PATTERN_HEXA,			//	38	8
PATTERN_DIGIT|PATTERN_HEXA,			//	39	9
PATTERN_PUNCTUATION,				//	3A	:
PATTERN_PUNCTUATION,				//	3B	;
PATTERN_PUNCTUATION,				//	3C	<
PATTERN_PUNCTUATION,				//	3D	=
PATTERN_PUNCTUATION,				//	3E	>
PATTERN_PUNCTUATION,				//	3F	?
PATTERN_PUNCTUATION,				//	40	@
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	41	A
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	42	B
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	43	C
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	44	D
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	45	E
PATTERN_LETTER|PATTERN_HEXA|PATTERN_UPPER,	//	46	F
PATTERN_LETTER|PATTERN_UPPER,			//	47	G
PATTERN_LETTER|PATTERN_UPPER,			//	48	H
PATTERN_LETTER|PATTERN_UPPER,			//	49	I
PATTERN_LETTER|PATTERN_UPPER,			//	4A	J
PATTERN_LETTER|PATTERN_UPPER,			//	4B	K
PATTERN_LETTER|PATTERN_UPPER,			//	4C	L
PATTERN_LETTER|PATTERN_UPPER,			//	4D	M
PATTERN_LETTER|PATTERN_UPPER,			//	4E	N
PATTERN_LETTER|PATTERN_UPPER,			//	4F	O
PATTERN_LETTER|PATTERN_UPPER,			//	50	P
PATTERN_LETTER|PATTERN_UPPER,			//	51	Q
PATTERN_LETTER|PATTERN_UPPER,			//	52	R
PATTERN_LETTER|PATTERN_UPPER,			//	53	S
PATTERN_LETTER|PATTERN_UPPER,			//	54	T
PATTERN_LETTER|PATTERN_UPPER,			//	55	U
PATTERN_LETTER|PATTERN_UPPER,			//	56	V
PATTERN_LETTER|PATTERN_UPPER,			//	57	W
PATTERN_LETTER|PATTERN_UPPER,			//	58	X
PATTERN_LETTER|PATTERN_UPPER,			//	59	Y
PATTERN_LETTER|PATTERN_UPPER,			//	5A	Z
PATTERN_PUNCTUATION,				//	5B	[
PATTERN_PUNCTUATION,				//	5C	'\'
PATTERN_PUNCTUATION,				//	5D	]
PATTERN_PUNCTUATION,				//	5E	^
PATTERN_PUNCTUATION,				//	5F	_
PATTERN_PUNCTUATION,				//	60	`
PATTERN_LETTER|PATTERN_HEXA,			//	61	a
PATTERN_LETTER|PATTERN_HEXA,			//	62	b
PATTERN_LETTER|PATTERN_HEXA,			//	63	c
PATTERN_LETTER|PATTERN_HEXA,			//	64	d
PATTERN_LETTER|PATTERN_HEXA,			//	65	e
PATTERN_LETTER|PATTERN_HEXA,			//	66	f
PATTERN_LETTER,					//	67	g
PATTERN_LETTER,					//	68	h
PATTERN_LETTER,					//	69	i
PATTERN_LETTER,					//	6A	j
PATTERN_LETTER,					//	6B	k
PATTERN_LETTER,					//	6C	l
PATTERN_LETTER,					//	6D	m
PATTERN_LETTER,					//	6E	n
PATTERN_LETTER,					//	6F	o
PATTERN_LETTER,					//	70	p
PATTERN_LETTER,					//	71	q
PATTERN_LETTER,					//	72	r
PATTERN_LETTER,					//	73	s
PATTERN_LETTER,					//	74	t
PATTERN_LETTER,					//	75	u
PATTERN_LETTER,					//	76	v
PATTERN_LETTER,					//	77	w
PATTERN_LETTER,					//	78	x
PATTERN_LETTER,					//	79	y
PATTERN_LETTER,					//	7A	z
PATTERN_PUNCTUATION,				//	7B	{
PATTERN_PUNCTUATION,				//	7C	|
PATTERN_PUNCTUATION,				//	7D	}
PATTERN_PUNCTUATION,				//	7E	~
PATTERN_CONTROL,				//	7F	DEL
};

static inline char get_specifier (int x) {
  if (x == '+') return RUN_ALL;
  if (x == '*') return RUN_ALL|RUN_ZERO;
  if (x == '?') return RUN_ONE|RUN_ZERO;
  return RUN_ONE;
}

static inline int match_char (const uint8_t * ptr, uint32_t matches, uint8_t * ch) {
  if ((matches & PATTERN_CUSTOM) && (ch[*ptr])) { return 1; }
  uint32_t flags = dmatch[*ptr];
  if ((matches & flags)) { return 1; }
  if ((matches & PATTERN_LOWER) && (flags & PATTERN_LETTER) && (flags & PATTERN_UPPER) == 0) return 0;
  if ((matches & PATTERN_NULL_CHAR) && (*ptr == '\0')) { return 1; }

  if (matches & (PATTERN_ALL)) {
    printf ("error unsupported formats\n");
  }

  return 0;
}

static inline int get_pattern (const uint8_t * fmt, uint32_t * matches, uint8_t * character) {
  if (*fmt != '%') {
    if (((*matches) & PATTERN_CUSTOM) == 0) {
      memset (character, 0, 256);
    }
    *matches |= PATTERN_CUSTOM;
    character[*fmt] = 1;
    return 1;
  }
  fmt++;
  switch (*fmt) {
  case 'a': *matches |=  PATTERN_LETTER;	break; // letters
  case 'A': *matches |= ~PATTERN_LETTER; 	break; // not all non letters

  case 's': *matches |=  PATTERN_SPACE;		break; // spaces
  case 'S': *matches |= ~PATTERN_SPACE;		break; // not spaces

  case 'c': *matches |=  PATTERN_CONTROL;	break; // control characters
  case 'C': *matches |= ~PATTERN_CONTROL;	break; // not control characters

  case 'd': *matches |=  PATTERN_DIGIT;		break; // digits
  case 'D': *matches |= ~PATTERN_DIGIT;		break; // not digits

  case 'l': *matches |=  PATTERN_LOWER;		break; // lower case
  case 'L': *matches |= ~PATTERN_LOWER;		break; // not lower case

  case 'u': *matches |=  PATTERN_UPPER;		break; // punctuation
  case 'U': *matches |= ~PATTERN_UPPER;		break; // not punctuation

  case 'p': *matches |=  PATTERN_PUNCTUATION;	break; // uppercase
  case 'P': *matches |= ~PATTERN_PUNCTUATION;	break; // not uppercase

  case 'w': *matches |=  PATTERN_ALPHANUM;	break; // alpha numeric
  case 'W': *matches |= ~PATTERN_ALPHANUM;	break; // not alpha numeric

  case 'x': *matches |=  PATTERN_HEXA;		break; // hexa numbers
  case 'X': *matches |= ~PATTERN_HEXA;		break; // not hexa numbers

  case 'z': *matches |=  PATTERN_NULL_CHAR;	break; // /0
  case 'Z': *matches |= ~PATTERN_NULL_CHAR; 	break; // not /0

  case 'y': *matches |=  PATTERN_ALL;		break; // TODO more testing
  case 'Y': *matches |= ~PATTERN_ALL;		break; // TODO more testing

  default:
    // add only the specific char to the buffer
    if (((*matches) & PATTERN_CUSTOM) == 0) {
      memset (character, 0, 256);
    }
    *matches |= PATTERN_CUSTOM;
    character[*fmt] = 1;
  break;
  }
  return 2;
}

static inline int match_pattern (const string_t *data, const string_t *pattern, int specifier) {
  const uint8_t * start_fmt = (uint8_t*)pattern->ptr;
  const uint8_t * max_fmt = (uint8_t*)pattern->ptr + pattern->len;
  const uint8_t * str = (uint8_t*)data->ptr;
  const uint8_t * max_str = (uint8_t*)data->ptr + data->len;
  uint32_t matches = 0;

  if (*start_fmt == '^') {
    matches |= PATTERN_NEGATIVE;
    start_fmt++;
  }

  uint8_t custom[256];
  for (const uint8_t * fmt = start_fmt; fmt < max_fmt;) {
    fmt += get_pattern (fmt, &matches, custom);
  }

  if ((matches & PATTERN_NEGATIVE) == 0) {
    if (str < max_str && match_char (str, matches, custom)) {
      str++;
      if (specifier & RUN_ALL) {
        while (str < max_str && match_char (str, matches, custom)) {
          str++;
        }
      }
    }
  } else {
    if (str < max_str && !match_char (str, matches, custom)) {
      str++;
      if (specifier & RUN_ALL) {
        while (str < max_str && !match_char (str, matches, custom)) {
          str++;
        }
      }
    }
  }

  size_t count = str - (uint8_t*)data->ptr;
  if (count == 0 && (specifier & RUN_ZERO) == 0) {
    return -1;
  }
  return count;
}

BASIC_EXPORT int match_string_virtual (string_t * data, uint32_t flags, const char *format, va_list argptr) {
  if (data->len <= 0) {
    if (*format == '\0') return 0;
    return -1;
  }
  string_t pattern, string;
  int used = 0;
  uint32_t specifier;
  const char * fmt = format;
  const char * ptr = data->ptr;
  const char * max = data->ptr + data->len;
  const char * start_capture = NULL;
  char error_char = 0;
  while (1) {
do_next:
    switch (*fmt) {
    case '%':
      pattern.ptr = (char*)(fmt++);
      fmt++;
      specifier = get_specifier (*fmt);
      pattern.len = 1;
      if (specifier != RUN_ONE) fmt++;

      string.ptr = (char*)ptr;
      string.len = max - ptr;

      used = match_pattern (&string, &pattern, specifier);
      if (used >= 0) { ptr += used; }
      else { return -1; }

      goto do_next;
    break;
    case '[':
      pattern.ptr = (char*)(++fmt);
      while (1) {
        if (*fmt == '\0') {
          error_char = ']';
          goto finalize;
        }
        if (*fmt == ']' && *(fmt-1) != '%') {
          break;
        }
        fmt++;
      }
      pattern.len = fmt - pattern.ptr;
      fmt++;

      specifier = get_specifier (*fmt);
      if (specifier != RUN_ONE) fmt++;

      string.ptr = (char*)ptr;
      string.len = max - ptr;

      used = match_pattern (&string, &pattern, specifier);
      if (used >= 0) { ptr += used; }
      else { return -1; }

      goto do_next;
    break;
    case '(':
      start_capture = ptr;
    break;
    case ')': {
      string_t * out = va_arg (argptr, string_t*);
      if (start_capture) {
        out->ptr = (char*)start_capture;
        out->len = ptr-start_capture;
        start_capture = NULL;
      } else {
        error_char = '(';
        goto finalize;
      }
    break; }
    case '\0':
      goto finalize;
    break;
    default:
      if (ptr >= max) { return -1; }
      if (flags & PATTERN_CASE) {
        if (toupper (*fmt) != toupper (*ptr)) { return -1; }
      } else {
        if (*fmt != *ptr) { return -1; }
      }
      ptr++;
    break;
    }
    fmt++;
  }
finalize:
  if (start_capture) {
    error_char = ')';
  }
  if (error_char) {
    fprintf (stderr, "error! missing '%c' in format '%s'\n", error_char, format);
    return -1;
  }
  used = ptr - data->ptr;
  if (used < 0) return -1;
  data->ptr += used;
  data->len -= used;
  return used;
}

BASIC_EXPORT int match_string (string_t * source, const char *format, ...) {
  va_list argptr;
  va_start (argptr, format);

  int used = match_string_virtual (source, 0, format, argptr);

  va_end (argptr);
  return used;
}

BASIC_EXPORT int matchi_string (string_t * source, const char *format, ...) {
  va_list argptr;
  va_start (argptr, format);

  int used = match_string_virtual (source, PATTERN_CASE, format, argptr);

  va_end (argptr);
  return used;
}

BASIC_EXPORT int match_string_equal (string_t * source, const char * format, ...) {
  va_list argptr;
  va_start (argptr, format);
  string_t orig = *source;

  int used = match_string_virtual (source, 0, format, argptr);

  va_end (argptr);

  *source = orig;
  if (used != (int)orig.len) {
    return -1;
  }

  return used;
}

BASIC_EXPORT int matchi_string_equal (string_t * source, const char * format, ...) {
  va_list argptr;
  va_start (argptr, format);
  string_t orig = *source;

  int used = match_string_virtual (source, PATTERN_CASE, format, argptr);

  va_end (argptr);

  *source = orig;
  if (used != (int)orig.len) {
    return -1;
  }

  return used;
}



