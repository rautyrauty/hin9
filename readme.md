hin9
====

hinsightd is a HTTP/1.1 webserver designed to be light on resources but not light on features. It focuses on getting things done in the simplest manner by using modern APIs like io\_uring and not forcing you to include the kitchen sink.

It has most of the features you expect out of more mature server: HTTP/1.1, ssl, reverse proxy, fastcgi and cgi, local caching for dynamic content, deflate compression, graceful restart; and then some exotic features like being a simple command line http downloader and a http library you can use in your own projects.

We use the Lua programming language to handle configuration, process requests and also to write 'plug-ins' directly inside the server configuration file. Example plug-ins that are left as an exercise to the user: custom logging formats, per vhost logging, different load balancing strategies, http authentication, url rewrites, timed actions like renewing let's encrypt certificates, etc.

more information [on the gitlab pages site](https://tiotags.gitlab.io/hinsightd) and a [detailed changelog](https://gitlab.com/tiotags/hin9/-/blob/master/docs/changelog.md) in the repo.

also check out the [multithreaded branch](https://gitlab.com/tiotags/hin10) that's way more stable than the main branch but has fewer features.

requirements
------------

* a recent linux kernel (>=5.6 - march 2020)
* liburing
* lua (5.1-5.4)
* libz
* optional: openssl/libressl, ffcall
* cmake or meson build systems for compilation

compile & run
-------------

`git clone https://gitlab.com/tiotags/hin9.git && cd hin9`

`mkdir -p build && cd build && meson --buildtype=release . .. && ninja && cd .. && build/hinsightd`

or

`mkdir -p build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make && cd .. && build/hinsightd`

download mode
-------------

you can also use the program as a HTTP/1.1 downloader
* download and print output: `build/hinsightd -d _url_`
* download and save to file: `build/hinsightd -do _url_ _path_`
* download multiple files: `build/hinsightd -dodo _url1_ _path1_ _url2_ _path2_`
* multiple files 2: `build/hinsightd -do _url1_ _path1_ -do _url2_ _out2_`
* download with progress bar: `build/hinsightd -dopdop _url1_ _path1_ _url2_ _path2_`




