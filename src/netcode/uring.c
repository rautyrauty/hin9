
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <liburing.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <linux/stat.h>

#include <basic_vfs.h> // needed for musl

#include "hin.h"
#include "hin_internal.h"
#include "conf.h"

#if HIN_URING_REDUCE_SYSCALLS
#define io_uring_submit1(x)
#else
#define io_uring_submit1(x) io_uring_submit(x);
#endif

typedef struct hin_sqe_que_struct {
  struct io_uring_sqe sqe;
  struct hin_sqe_que_struct * next;
} hin_sqe_que_t;

typedef struct {
  int ret;
  hin_buffer_t * buffer;
  basic_dlist_t list;
} hin_callback_que_t;

static HIN_LOCAL_VAR struct io_uring ring;
static HIN_LOCAL_VAR hin_sqe_que_t * sqe_queue = NULL;
static HIN_LOCAL_VAR basic_dlist_t callback_que = {NULL, NULL};

int hin_request_callback (hin_buffer_t * buf, int ret) {
  if (ret == -1) ret = -errno;

  hin_callback_que_t * new = calloc (1, sizeof (*new));
  new->ret = ret;
  new->buffer = buf;
  basic_dlist_append (&callback_que, &new->list);

  return 0;
}

int hin_request_callback_run (hin_buffer_t * buf, int ret) {
  buf->flags &= ~HIN_ACTIVE;
  if (buf->flags & HIN_INACTIVE) {
    hin_buffer_clean (buf);
    return 0;
  }

  int err = buf->callback (buf, ret);
  if (err) {
    hin_buffer_clean (buf);
  }

  return 0;
}

static void hin_request_callback_process () {
  basic_dlist_t * elem = callback_que.next;
  while (elem) {
    hin_callback_que_t * que = basic_dlist_ptr (elem, offsetof (hin_callback_que_t, list));
    elem = elem->next;

    hin_request_callback_run (que->buffer, que->ret);

    basic_dlist_remove (&callback_que, &que->list);
    free (que);
  }
}

static int hin_request_active (hin_buffer_t * buf) {
  if (buf->flags & HIN_ACTIVE) {
    hin_error ("buf %d already active", buf->fd);
    return -1;
  }
  buf->flags |= HIN_ACTIVE;
  if (hin_g.flags & HIN_FORCE_EPOLL) {
    buf->flags |= HIN_EPOLL;
  }
  if ((buf->flags & HIN_EPOLL) && (buf->flags & HIN_FILE)) {
    buf->flags |= HIN_SYNC;
    buf->flags &= ~HIN_EPOLL;
  }
  return 0;
}

static inline struct io_uring_sqe * hin_request_sqe (hin_buffer_t * buf) {
  struct io_uring_sqe *sqe = NULL;

  if (ring.ring_fd > 0)
    sqe = io_uring_get_sqe (&ring);

  if (sqe == NULL) {
    hin_sqe_que_t * new = calloc (1, sizeof (*new));
    sqe = &new->sqe;
    new->next = sqe_queue;
    sqe_queue = new;
  }

  if (buf->flags & HIN_LINKED_REQ) {
    sqe->flags |= IOSQE_IO_LINK;
  }

  return sqe;
}

static inline void hin_process_sqe_queue () {
  hin_sqe_que_t * new = sqe_queue;
  if (new == NULL) return ;
  struct io_uring_sqe * sqe = io_uring_get_sqe (&ring);
  if (sqe == NULL) return ;
  *sqe = new->sqe;
  sqe_queue = new->next;
  free (new);
}

HIN_EXPORT int hin_request_is_overloaded () {
  if (sqe_queue) { return 1; }
  return 0;
}

HIN_EXPORT int hin_request_write (hin_buffer_t * buffer) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req %s%s \tbuf %p cb %p fd %d\n", buffer->flags & HIN_SOCKET ? "send" : "write", buffer->flags & HIN_SSL ? "s" : "", buffer, buffer->callback, buffer->fd);

  if (buffer->flags & HIN_SSL) {
    return hin_ssl_request_write (buffer);
  }

  if (buffer->flags & HIN_EPOLL) {
    return hin_epoll_request_write (buffer);
  }

  if (buffer->flags & HIN_SYNC) {
    int ret;
    if (buffer->flags & HIN_OFFSETS) {
      ret = pwrite (buffer->fd, buffer->ptr, buffer->count, buffer->pos);
    } else {
      ret = write (buffer->fd, buffer->ptr, buffer->count);
    }
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);

  if (buffer->flags & HIN_SOCKET) {
    io_uring_prep_send (sqe, buffer->fd, buffer->ptr, buffer->count, 0);
  } else {
    io_uring_prep_write (sqe, buffer->fd, buffer->ptr, buffer->count, buffer->pos);
  }

  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_read (hin_buffer_t * buffer) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req %s%s \tbuf %p cb %p fd %d\n", buffer->flags & HIN_SOCKET ? "recv" : "read", buffer->flags & HIN_SSL ? "s" : "", buffer, buffer->callback, buffer->fd);

  if (buffer->flags & HIN_SSL) {
    return hin_ssl_request_read (buffer);
  }

  if (buffer->flags & HIN_EPOLL) {
    return hin_epoll_request_read (buffer);
  }

  if (buffer->flags & HIN_SYNC) {
    int ret;
    if (buffer->flags & HIN_OFFSETS) {
      ret = pread (buffer->fd, buffer->ptr, buffer->count, buffer->pos);
    } else {
      ret = read (buffer->fd, buffer->ptr, buffer->count);
    }
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);

  if (buffer->flags & HIN_SOCKET) {
    io_uring_prep_recv (sqe, buffer->fd, buffer->ptr, buffer->count, 0);
  } else {
    io_uring_prep_read (sqe, buffer->fd, buffer->ptr, buffer->count, buffer->pos);
  }

  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_accept (hin_buffer_t * buffer, int flags) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req accept \tbuf %p cb %p fd %d\n", buffer, buffer->callback, buffer->fd);

  hin_client_t * client = (hin_client_t*)buffer->parent;
  hin_client_t * server = (hin_client_t*)client->parent;
  client->ai_addrlen = sizeof (client->ai_addr);

  if (buffer->flags & HIN_EPOLL) {
    return hin_epoll_request_accept (buffer);
  }

  if (buffer->flags & HIN_SYNC) {
    int ret = accept4 (buffer->fd, &client->ai_addr, &client->ai_addrlen, flags);
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  io_uring_prep_accept (sqe, server->sockfd, &client->ai_addr, &client->ai_addrlen, flags);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_connect (hin_buffer_t * buffer, struct sockaddr * ai_addr, int ai_addrlen) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req connect \tbuf %p cb %p\n", buffer, buffer->callback);

  if (buffer->flags & HIN_EPOLL) {
    return hin_epoll_request_connect (buffer);
  }
  if (buffer->flags & HIN_SYNC) {
    int ret = connect (buffer->fd, ai_addr, ai_addrlen);
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  io_uring_prep_connect (sqe, buffer->fd, ai_addr, ai_addrlen);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_close (hin_buffer_t * buffer) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req close \tbuf %p cb %p fd %d\n", buffer, buffer->callback, buffer->fd);

  //if (buffer->flags & HIN_SSL) {
  //  return hin_ssl_request_close (buffer);
  //}

  if (buffer->flags & (HIN_SYNC | HIN_EPOLL)) {
    int ret = close (buffer->fd);
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  io_uring_prep_close (sqe, buffer->fd);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_openat (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mode) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req open \tbuf %p cb %p\n", buffer, buffer->callback);

  if (buffer->flags & (HIN_SYNC | HIN_EPOLL)) {
    int ret = openat (dfd, path, flags, mode);
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  io_uring_prep_openat (sqe, dfd, path, flags, mode);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_timeout (hin_buffer_t * buffer, struct __kernel_timespec * ts, int count, int flags) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req time \tbuf %p cb %p\n", buffer, buffer->callback);

  if (buffer->flags & (HIN_SYNC | HIN_EPOLL)) {
    hin_error ("timeout can't be sync/epoll");
    return -1;
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  io_uring_prep_timeout (sqe, ts, count, flags);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

HIN_EXPORT int hin_request_statx (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mask) {
  if (hin_request_active (buffer)) return -1;

  if (buffer->debug & HNDBG_URING)
    hin_debug (" req stat \tbuf %p cb %p\n", buffer, buffer->callback);

  if (buffer->flags & (HIN_SYNC | HIN_EPOLL)) {
    int ret = statx (dfd, path, flags, mask, (struct statx *)buffer->ptr);
    return hin_request_callback (buffer, ret);
  }

  struct io_uring_sqe *sqe = hin_request_sqe (buffer);
  if (buffer->count < (int)sizeof (struct statx)) {
    hin_error ("statx insufficient buf");
    return -1;
  }
  io_uring_prep_statx (sqe, dfd, path, flags, mask, (struct statx *)buffer->ptr);
  io_uring_sqe_set_data (sqe, buffer);
  io_uring_submit1 (&ring);

  return 0;
}

int hin_event_init () {
  struct io_uring_params params;
  memset (&params, 0, sizeof params);

  //params.flags = (IORING_SETUP_SINGLE_ISSUER | IORING_SETUP_DEFER_TASKRUN);

  if (hin_g.flags & HIN_FORCE_EPOLL) {
    hin_debug ("hin force using epoll didn't initialize uring\n");
    return -1;
  }

  if (ring.ring_fd > 0)
    io_uring_queue_exit (&ring);

  memset (&ring, 0, sizeof (ring));
  int err = io_uring_queue_init_params (HIN_URING_QUEUE_DEPTH, &ring, &params);
  if (err < 0) {
    hin_error ("io_uring_queue_init %s", strerror (-err));
    return -1;
  }
  #if HIN_URING_DONT_FORK
  err = io_uring_ring_dontfork (&ring);
  if (err < 0) {
    hin_error ("io_uring_ring_dontfork %s", strerror (-err));
    return -1;
  }
  #endif

  hin_process_sqe_queue ();

  return 1;
}

int hin_event_clean () {
  if (ring.ring_fd > 0)
    io_uring_queue_exit (&ring);

  memset (&ring, 0, sizeof (ring));

  return 0;
}

static void hin_event_check_new () {
  #ifdef HIN_URING_REDUCE_SYSCALLS
  io_uring_submit (&ring);
  #endif
  hin_process_sqe_queue ();
  hin_request_callback_process ();
}

HIN_EXPORT int hin_event_wait () {
  struct io_uring_cqe *cqe;

  for (int nevents = 0; nevents < HIN_URING_EVENTS_PER_WAIT; nevents++) {
    int err = io_uring_peek_cqe (&ring, &cqe);
    if (err) {
      if (err == -EAGAIN) {
        hin_event_check_new ();
        if (nevents != 0) return 1;
        io_uring_wait_cqe (&ring, &cqe);
        continue;
      }
      if (err == -EINTR) {
        continue;
      }
      hin_error ("io_uring_wait_cqe: %s", strerror (-err));
      io_uring_cqe_seen (&ring, cqe);
      continue;
    }

    hin_buffer_t * buffer = (hin_buffer_t *)cqe->user_data;

    uint32_t debug = buffer->debug;
    if (debug & HNDBG_URING) hin_debug ("req begin buf %p cb %p %d\n", buffer, buffer->callback, cqe->res);

    hin_request_callback_run (buffer, cqe->res);

    io_uring_cqe_seen (&ring, cqe);

    if (debug & HNDBG_URING) hin_debug ("req done. buf %p %d\n", buffer, err);

    if (hin_check_alive () == 0) { return 0; }
  }

  hin_event_check_new ();
  return 1;
}

HIN_EXPORT void hin_event_loop () {
  if (hin_g.flags & HIN_FORCE_EPOLL) {
    while (hin_check_alive ()) {
      hin_request_callback_process ();
      hin_epoll_check ();
      hin_request_callback_process ();
      hin_timer_process ();
    }
    return ;
  }

  while (hin_event_wait ()) {
    hin_event_process ();
  }
}

