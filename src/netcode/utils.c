
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#include "hin.h"
#include "hin_internal.h"
#include "listen.h"

HIN_EXPORT int hin_client_addr (char * str, int len, struct sockaddr * ai_addr, socklen_t ai_addrlen) {
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  int err = getnameinfo (ai_addr, ai_addrlen,
			hbuf, sizeof hbuf,
			sbuf, sizeof sbuf,
			NI_NUMERICHOST | NI_NUMERICSERV);
  if (err) {
    hin_error ("getnameinfo: %s", gai_strerror (err));
    return -1;
  }
  snprintf (str, len, "%s:%s", hbuf, sbuf);
  return 0;
}


int hin_make_socket_non_blocking (int sockfd) {
  int flags, s;

  flags = fcntl (sockfd, F_GETFL, 0);
  if (flags == -1) {
    hin_perror ("fcntl");
    return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl (sockfd, F_SETFL, flags);
  if (s == -1) {
    hin_perror ("fcntl");
    return -1;
  }

  return 0;
}

HIN_EXPORT void hin_error (const char * fmt, ...) {
  va_list ap;
  va_start (ap, fmt);

  fprintf (stderr, "error! ");
  vfprintf (stderr, fmt, ap);
  fprintf (stderr, "\n");

  va_end (ap);
}

HIN_EXPORT void hin_perror (const char * fmt, ...) {
  va_list ap;
  va_start (ap, fmt);

  fprintf (stderr, "error! ");
  vfprintf (stderr, fmt, ap);
  fprintf (stderr, ": %s\n", strerror (errno));

  va_end (ap);
}

HIN_EXPORT void hin_weird_error (int errcode) {
  fprintf (stderr, "error! generic error # %d please consult source code\n", (errcode));
}



