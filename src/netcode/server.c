
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include "hin.h"
#include "hin_internal.h"
#include "listen.h"

HIN_EXPORT void hin_client_close (hin_client_t * client) {
  if (hin_g.debug & HNDBG_SOCKET)
    hin_debug ("socket %d close\n", client->sockfd);

  hin_server_t * server = (hin_server_t*)client->parent;

  hin_g.num_client--;

  basic_dlist_remove (&server->client_list, &client->list);

  if (client->flags & HIN_SSL)
    hin_client_ssl_cleanup (client);
  free (client);

  if (server->client_list.next == NULL && server->accept_list.next == NULL) {
    hin_server_close (server);
  }
}

static int hin_server_do_client_callback (hin_client_t * client) {
  hin_server_t * server = (hin_server_t*)client->parent;

  if (server->ssl_ctx) {
    hin_ssl_accept_init (client);
  }

  int ret = 0;
  if (server->accept_callback) {
    ret = server->accept_callback (client);
  }

  server->num_client++;
  hin_g.num_client++;

  return ret;
}

static hin_client_t * hin_server_create_client (hin_server_t * server) {
  hin_client_t * new = calloc (1, sizeof (hin_client_t) + server->user_data_size);
  new->type = HIN_CLIENT;
  new->magic = HIN_CLIENT_MAGIC;
  new->parent = server;
  return new;
}

static void hin_server_accept_buffer_free (hin_buffer_t * buf) {
  hin_client_t * client = (hin_client_t*)buf->parent;
  hin_server_t * server = (hin_server_t*)client->parent;
  if (client->sockfd > 0) close (client->sockfd);
  free (client);		// free empty client
  buf->parent = NULL;
  basic_dlist_remove (&server->accept_list, &buf->list);
  buf->flags &= ~HIN_ACTIVE;
  hin_buffer_clean (buf);
}

static int hin_server_accept_callback (hin_buffer_t * buffer, int ret) {
  hin_client_t * client = (hin_client_t*)buffer->parent;
  hin_server_t * server = (hin_server_t*)client->parent;

  if (buffer->flags & HIN_INACTIVE) {
    // TODO free virtual client
    hin_server_accept_buffer_free (buffer);
    if (ret > 0) close (ret);
    return 0;
  }

  if (ret < 0) {
    switch (-ret) {
    case EAGAIN:
    #if EAGAIN != EWOULDBLOCK
    case EWOULDBLOCK:
    #endif
    case EINTR:
    case ECONNABORTED:
    case ENETDOWN:
    case EPROTO:
    case EHOSTDOWN:
    case ENONET:
    case EHOSTUNREACH:
    case ENETUNREACH:
      // retry errors
      if (hin_request_accept (buffer, server->accept_flags) < 0) {
        hin_weird_error (5364567);
        return -1;
      }
      return 0;
    break;
    case EMFILE:
    case ENFILE:
    case ENOBUFS:
    case ENOMEM:
    case EPERM:
      // slow down errors
      if (hin_g.debug & (HNDBG_INFO|HNDBG_CONFIG|HNDBG_RW_ERROR))
        hin_error ("accept %d ran out of resources: %s", server->c.sockfd, strerror (-ret));
      return 0;
    break;
    default:
      // other errors are fatal
      hin_error ("failed accept %d '%s'", server->c.sockfd, strerror (-ret));
      hin_server_accept_buffer_free (buffer);
      // TODO do you need to clean the rest too ?
      return 0;
    break;
    }
  }

  if (hin_g.debug & HNDBG_SOCKET) {
    char buf1[1080];
    hin_client_addr (buf1, sizeof buf1, &client->ai_addr, client->ai_addrlen);
    hin_debug ("socket %d accept '%s' server fd %d time %lld\n", ret, buf1, server->c.sockfd, (long long)time (NULL));
  }

  client->sockfd = ret;
  if (hin_server_do_client_callback (client) < 0) {
    if (hin_request_accept (buffer, server->accept_flags) < 0) {
      hin_weird_error (3252543);
      return -1;
    }
    // not really an error but it's unexpected atm
    hin_weird_error (432526654);
    return 0;
  }

  if (buffer->flags & HIN_INACTIVE) {
    hin_server_accept_buffer_free (buffer);
    return 0;
  }

  basic_dlist_append (&server->client_list, &client->list);

  hin_client_t * new = hin_server_create_client (client->parent);
  buffer->parent = new;

  if (hin_request_accept (buffer, server->accept_flags) < 0) {
    hin_weird_error (435332);
    return -1;
  }
  return 0;
}

int hin_server_reaccept () {
  basic_dlist_t * elem = hin_g.server_list.next;
  if (elem == NULL) {
    //hin_check_alive ();
  }
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    basic_dlist_t * elem1 = server->accept_list.next;
    while (elem1) {
      hin_buffer_t * buf = basic_dlist_ptr (elem1, offsetof (hin_buffer_t, list));
      elem1 = elem1->next;

      if (buf == NULL) continue;
      if (buf->flags & HIN_INACTIVE) continue;
      if (buf->flags & HIN_ACTIVE) continue;
      if (hin_request_accept (buf, server->accept_flags) < 0) {
        hin_weird_error (436332);
        return -1;
      }
    }
  }
  return 0;
}

int hin_server_start_accept (hin_server_t * server) {
  if (server->c.sockfd < 0) {
    if (server->flags & HIN_SERVER_RETRY) { return 0; }
    hin_error ("didn't listen first");
    return -1;
  }

  hin_buffer_t * buffer = calloc (1, sizeof *buffer);
  buffer->fd = server->c.sockfd;
  buffer->parent = hin_server_create_client (server);
  buffer->callback = hin_server_accept_callback;
  buffer->debug = server->debug;

  basic_dlist_append (&server->accept_list, &buffer->list);

  if ((server->c.flags & HIN_SERVER_INIT) == 0) {
    basic_dlist_append (&hin_g.server_list, &server->c.list);
    server->c.flags |= HIN_SERVER_INIT;
  }

  if (hin_request_accept (buffer, server->accept_flags) < 0) {
    hin_weird_error (35776893);
    return -1;
  }

  return 0;
}

HIN_EXPORT int hin_server_close (hin_server_t * server) {
  basic_dlist_t * elem = server->accept_list.next;
  while (elem) {
    hin_buffer_t * buf = basic_dlist_ptr (elem, offsetof (hin_buffer_t, list));
    elem = elem->next;

    hin_server_accept_buffer_free (buf);
  }

  if (server->client_list.next) return 0;

  hin_g.num_listen--;
  basic_dlist_remove (&hin_g.server_list, &server->c.list);

  elem = server->accept_list.next;
  while (elem) {
    hin_buffer_t * buf = basic_dlist_ptr (elem, offsetof (hin_buffer_t, list));
    elem = elem->next;

    hin_server_accept_buffer_free (buf);
  }

  close (server->c.sockfd);
  free (server);

  hin_check_alive ();

  return 0;
}



