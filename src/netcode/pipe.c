
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <netinet/tcp.h>

#include "hin.h"
#include "hin_internal.h"
#include "conf.h"

#define hin_buffer_pipe_dlist_ptr(elem) (basic_dlist_ptr (elem, offsetof (hin_buffer_t, pipe_dlist)))

static int hin_pipe_read_callback (hin_buffer_t * buffer, int ret);
static int hin_pipe_write_callback (hin_buffer_t * buffer, int ret);
int hin_pipe_copy_raw (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
hin_buffer_t * hin_pipe_get_buffer (hin_pipe_t * pipe, int sz);

static void hin_pipe_close (hin_pipe_t * pipe) {
  if (pipe->debug & HNDBG_PIPE) hin_debug ("pipe %d>%d close read %d write %d\n", pipe->in.fd, pipe->out.fd, (pipe->in.flags & HIN_DONE), (pipe->out.flags & HIN_DONE));

  if (pipe->flags & HIN_CORK) {
    int state = 0;
    setsockopt (pipe->out.fd, IPPROTO_TCP, TCP_CORK, &state, sizeof (state));
    pipe->flags &= ~HIN_CORK;
  }

  pipe->in.flags |= HIN_DONE|HIN_INACTIVE;
  pipe->out.flags |= HIN_DONE|HIN_INACTIVE;

  if (pipe->finish_callback) {
    pipe->finish_callback (pipe);
    pipe->finish_callback = NULL;
  }

  pipe->out_error_callback = NULL;
  pipe->in_error_callback = NULL;
  pipe->in.fd = -1;
  pipe->out.fd = -1;

  basic_dlist_t * elem = pipe->write_que.next;
  while (elem) {
    hin_buffer_t * buf = hin_buffer_pipe_dlist_ptr (elem);
    elem = elem->next;

    basic_dlist_remove (&pipe->write_que, &buf->pipe_dlist);
    hin_buffer_stop_clean (buf);
  }

  elem = pipe->writing.next;
  while (elem) {
    hin_buffer_t * buf = hin_buffer_pipe_dlist_ptr (elem);
    elem = elem->next;

    basic_dlist_remove (&pipe->writing, &buf->pipe_dlist);
    hin_buffer_stop_clean (buf);
  }

  elem = pipe->reading.next;
  while (elem) {
    hin_buffer_t * buf = hin_buffer_pipe_dlist_ptr (elem);
    elem = elem->next;

    basic_dlist_remove (&pipe->reading, &buf->pipe_dlist);
    hin_buffer_stop_clean (buf);
  }

  if (pipe->extra) {
    free (pipe->extra);
    pipe->extra = NULL;
  }

  free (pipe);
}

static int hin_pipe_check_limits (hin_pipe_t * pipe) {
  if ((pipe->in.flags & HIN_COUNT) && pipe->in.count <= 0) {
    if (pipe->debug & HNDBG_PIPE) hin_debug ("pipe %d>%d read finished\n", pipe->in.fd, pipe->out.fd);
    pipe->in.flags |= HIN_DONE;
    return 1;
  }
  if (pipe->in.flags & HIN_DONE) {
    return 1;
  }
  return 0;
}

HIN_EXPORT int hin_pipe_init (hin_pipe_t * pipe) {
  if (pipe->read_callback == NULL)
    pipe->read_callback = hin_pipe_copy_raw;
  if (pipe->buffer_callback == NULL)
    pipe->buffer_callback = hin_pipe_get_buffer;

  pipe->in.count = 0;
  pipe->out.count = 0;
  pipe->in.pos = pipe->out.pos = 0;
  return 0;
}

HIN_EXPORT int hin_pipe_start (hin_pipe_t * pipe) {
  if (pipe->flags & HIN_HASH) {
    pipe->hash = 6883;
  }
  pipe->in.count += pipe->sz;
  pipe->in.flags &= ~HIN_DONE;
  hin_pipe_check_limits (pipe);

  if ((pipe->sz > hin_g.cork_size) && (pipe->out.flags & HIN_SOCKET)) {
    int state = 1;
    setsockopt (pipe->out.fd, IPPROTO_TCP, TCP_CORK, &state, sizeof (state));
    pipe->flags |= HIN_CORK;
    pipe->flags &= ~HIN_CONDENSE;
  }

  if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d start %lld/%lld bytes cork %d\n",
      pipe->in.fd, pipe->out.fd,
      (long long)(pipe->sz - pipe->in.count),
      (long long)pipe->sz,
      !!(pipe->flags & HIN_CORK)
    );

  hin_pipe_advance (pipe);
  return 0;
}

HIN_EXPORT hin_buffer_t * hin_pipe_get_buffer (hin_pipe_t * pipe, int sz) {
  hin_buffer_t * buf = malloc (sizeof *buf + sz);
  memset (buf, 0, sizeof (*buf));
  buf->flags = 0;
  buf->ptr = buf->buffer;
  buf->count = buf->sz = sz;

  buf->flags |= HIN_COUNT;
  buf->parent = (void*)pipe;
  buf->debug = pipe->debug;
  return buf;
}

static int hin_pipe_count_que (hin_buffer_t * buf) {
  int queued = 1;

  basic_dlist_t *elem = buf->pipe_dlist.next;
  while (elem) {
    elem = elem->next;

    queued++;
  }

  return queued;
}

HIN_EXPORT int hin_pipe_append_raw (hin_pipe_t * pipe, hin_buffer_t * buf) {
  pipe->num_que += hin_pipe_count_que (buf);
  pipe->write_que_size += buf->count;
  basic_dlist_append (&pipe->write_que, &buf->pipe_dlist);
  return 0;
}

HIN_EXPORT int hin_pipe_prepend_raw (hin_pipe_t * pipe, hin_buffer_t * buf) {
  pipe->num_que += hin_pipe_count_que (buf);
  pipe->write_que_size += buf->count;
  basic_dlist_prepend (&pipe->write_que, &buf->pipe_dlist);
  return 0;
}

static int hin_pipe_process_buffer (hin_pipe_t * pipe, hin_buffer_t * buffer, uint32_t flags) {
  if (pipe->out.flags & HIN_INACTIVE) {
    hin_buffer_clean (buffer);
    return 0;
  }

  if (pipe->in.flags & HIN_OFFSETS) {
    //pipe->in.pos += buffer->count; // needs to be done in the read next function
  }

  if (flags & HIN_PIPE_COUNT)
    buffer->flags |= HIN_COUNT;

  int ret1 = 0, flush = (buffer->flags & HIN_DONE) ? 1 : 0;

  if (pipe->debug & HNDBG_PIPE) hin_debug ("pipe %d>%d append %d bytes %s\n", pipe->in.fd, pipe->out.fd, buffer->count, flush ? "flush" : "cont");

  if (pipe->decode_callback) {
    ret1 = pipe->decode_callback (pipe, buffer, buffer->count, flush);
  } else {
    ret1 = pipe->read_callback (pipe, buffer, buffer->count, flush);
  }

  if (ret1) hin_buffer_clean (buffer);
  return 0;
}

HIN_EXPORT int hin_pipe_write_process (hin_pipe_t * pipe, hin_buffer_t * buffer, uint32_t flags) {
  basic_dlist_t * elem = &buffer->pipe_dlist;
  while (elem) {
    hin_buffer_t * buf = hin_buffer_pipe_dlist_ptr (elem);
    elem = elem->next;

    if (pipe->in.flags & HIN_OFFSETS) {
      pipe->in.pos += buf->count;
    }

    if (pipe->in.flags & HIN_COUNT) {
      pipe->in.count -= buf->count;
    }

    if (hin_pipe_check_limits (pipe)) {
      buf->flags |= HIN_DONE;
    }

    pipe->num_que++;

    hin_pipe_process_buffer (pipe, buf, flags);
  }
  return 0;
}

HIN_EXPORT int hin_pipe_finish (hin_pipe_t * pipe) {
  pipe->in.flags |= HIN_DONE;

  hin_buffer_t * buf = hin_buffer_create_from_data (pipe, NULL, 0);
  buf->flags |= HIN_DONE;
  hin_pipe_process_buffer (pipe, buf, HIN_PIPE_ALL);

  hin_pipe_advance (pipe);

  return 0;
}

HIN_EXPORT int hin_pipe_copy_raw (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush) {
  if (num <= 0) { return 1; }
  buffer->count = num;
  //printf ("pipe %d>%d write %ld/%ld pos %ld\n", pipe->in.fd, pipe->out.fd, buffer->count, buffer->count, buffer->pos);
  hin_pipe_append_raw (pipe, buffer);
  //if (flush) return 1; // already cleaned in the write done handler
  return 0;
}

static int hin_pipe_condense (hin_pipe_t * pipe, hin_buffer_t * buffer) {
  basic_dlist_t * elem = pipe->write_que.next->next;
  if (elem == NULL && (pipe->in.flags & HIN_DONE) == 0) {
    return 1;
  }
  if (elem && hin_g.debug & HNDBG_HTTP)
    hin_debug ("httpd %d condense %d/%d\n", buffer->fd, buffer->count, buffer->sz);

  while (elem) {
    hin_buffer_t * buf = hin_buffer_pipe_dlist_ptr (elem);
    elem = elem->next;

    int len = buffer->sz - buffer->count;
    if (len > buf->count) len = buf->count;
    else if (len == 0) break;

    if (buffer->ptr < buffer->buffer || buffer->ptr > buffer->buffer + buffer->sz) {
      hin_error ("condense using a dynamic buffer not supported");
    } else if (buffer->ptr != buffer->buffer) {
      memmove (buffer->buffer, buffer->ptr, buffer->count);
      buffer->ptr = buffer->buffer;
    }

    memcpy (&buffer->ptr[buffer->count], buf->ptr, len);
    buf->ptr += len;
    buf->count -= len;
    buffer->count += len;
    if (buf->count == 0) {
      basic_dlist_remove (&pipe->write_que, &buf->pipe_dlist);
      pipe->num_que--;
      pipe->write_que_size -= buf->count;
      hin_buffer_clean (buf);
    }
  }

  return 0;
}

static int hin_pipe_read_next (hin_pipe_t * pipe) {
  if (pipe->in.flags & HIN_INACTIVE) return 0;
  if (hin_pipe_check_limits (pipe)) {
    return 0;
  }
  int ret = 1;

  int sz = READ_SZ;
  if (pipe->in.flags & HIN_COUNT) {
    int extra = 0;
    if (pipe->flags & HIN_CONDENSE) extra = pipe->write_que_size;
    if (pipe->in.count + extra < sz) sz = pipe->in.count + extra;
  }
  hin_buffer_t * buffer = pipe->buffer_callback (pipe, sz);

  buffer->fd = pipe->in.fd;
  buffer->ssl = pipe->in.ssl;
  buffer->flags = (buffer->flags & HIN_COUNT) | (pipe->in.flags & (~HIN_COUNT));
  buffer->parent = pipe;

  buffer->callback = hin_pipe_read_callback;
  buffer->count = buffer->sz;

  if ((pipe->flags & HIN_CONDENSE)
  && (pipe->write_que.next)) {
    int count = pipe->write_que_size;
    if (count > 0 && count < buffer->sz) {
      buffer->count = buffer->sz - count;
      if (hin_g.debug & HNDBG_HTTP)
        hin_debug ("httpd %d condense request %d\n", buffer->fd, buffer->count);
      ret = 0;
    }
  }

  if (pipe->in.flags & HIN_COUNT) {
    pipe->in.count -= buffer->count;
  }

  if (buffer->flags & HIN_OFFSETS) {
    buffer->pos = pipe->in.pos;
    pipe->in.pos += buffer->count;
  }

  buffer->pipe_dlist.next = buffer->pipe_dlist.prev = NULL;
  basic_dlist_append (&pipe->reading, &buffer->pipe_dlist);
  pipe->num_que++;

  if (hin_request_read (buffer) < 0) {
    if (pipe->in_error_callback)
      pipe->in_error_callback (pipe, 0);
    hin_pipe_close (pipe);
    hin_buffer_clean (buffer);
    return -1;
  }

  if ((pipe->in.flags & HIN_COUNT) && pipe->in.count <= 0) {
    buffer->flags |= HIN_DONE;
    return 0;
  }

  return ret;
}

static int hin_pipe_write_next (hin_pipe_t * pipe, hin_buffer_t * buffer) {
  if ((pipe->flags & HIN_CONDENSE) && (buffer->count < buffer->sz)) {
    if (hin_pipe_condense (pipe, buffer)) {
      return 0;
    }
  }

  buffer->fd = pipe->out.fd;
  buffer->ssl = pipe->out.ssl;
  //buffer->flags = pipe->out.flags;
  buffer->flags = (buffer->flags & HIN_COUNT) | (pipe->out.flags & (~HIN_COUNT));
  buffer->callback = hin_pipe_write_callback;
  buffer->parent = pipe;

  if (pipe->out.flags & HIN_OFFSETS) {
    buffer->pos = pipe->out.pos;
    pipe->out.pos += buffer->count;
  }

  if (hin_request_write (buffer) < 0) {
    if (pipe->out_error_callback)
      pipe->out_error_callback (pipe, 0);
    hin_pipe_close (pipe);
    return -1;
  }

  pipe->write_que_size -= buffer->count;

  basic_dlist_remove (&pipe->write_que, &buffer->pipe_dlist);
  basic_dlist_append (&pipe->writing, &buffer->pipe_dlist);

  return 1;
}

HIN_EXPORT int hin_pipe_advance (hin_pipe_t * pipe) {
  while ((pipe->in.flags & (HIN_DONE|HIN_INACTIVE)) == 0
  && (pipe->num_que < HIN_PIPE_QUE_DEPTH)
  && ((pipe->reading.next == NULL))) {
    if (hin_pipe_read_next (pipe) <= 0) {
      break;
    }
  }

  while (pipe->write_que.next
  && (pipe->writing.next == NULL)) {
    hin_buffer_t * buffer = hin_buffer_pipe_dlist_ptr (pipe->write_que.next);
    if (hin_pipe_write_next (pipe, buffer) <= 0) break;
  }

  if (pipe->write_que.next == NULL
  && pipe->writing.next == NULL
  && pipe->reading.next == NULL
  && ((pipe->in.flags) & HIN_DONE)) {
    hin_pipe_close (pipe);
    return 0;
  }

  return 0;
}

static int hin_pipe_write_callback (hin_buffer_t * buffer, int ret) {
  hin_pipe_t * pipe = (hin_pipe_t*)buffer->parent;

  if (ret < 0) {
    hin_error ("pipe %d>%d write error '%s'", pipe->in.fd, pipe->out.fd, strerror (-ret));
    basic_dlist_remove (&pipe->writing, &buffer->pipe_dlist);
    pipe->num_que--;
    if (pipe->out_error_callback)
      pipe->out_error_callback (pipe, ret);
    hin_pipe_close (pipe);
    return -1;
  }

  pipe->out.count += ret;

  if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d write %d/%d pos %lld count %lld\n", pipe->in.fd, pipe->out.fd, ret, buffer->count, (long long)pipe->out.pos, (long long)pipe->out.count);

  if (pipe->flags & HIN_HASH) {
    uint8_t * start = (uint8_t*)buffer->ptr;
    uint8_t * max = start + ret;
    while (start < max) {
      int c = *start++;
      pipe->hash = ((pipe->hash << 3) + pipe->hash) + c;
    }
  }

  if (ret < buffer->count) {
    if (pipe->debug & HNDBG_PIPE) hin_debug ("pipe %d>%d write incomplete %d/%d\n", pipe->in.fd, pipe->out.fd, ret, buffer->count);

    buffer->ptr += ret;
    buffer->count -= ret;
    if (buffer->flags & HIN_OFFSETS)
      buffer->pos += ret;

    if (hin_request_write (buffer) < 0) {
      basic_dlist_remove (&pipe->writing, &buffer->pipe_dlist);
      pipe->num_que--;
      if (pipe->out_error_callback)
        pipe->out_error_callback (pipe, 0);
      hin_pipe_close (pipe);
      return -1;
    }
    return 0;
  }

  basic_dlist_remove (&pipe->writing, &buffer->pipe_dlist);
  pipe->num_que--;
  hin_pipe_advance (pipe);

  return 1;
}

static int hin_pipe_read_callback (hin_buffer_t * buffer, int ret) {
  hin_pipe_t * pipe = (hin_pipe_t*)buffer->parent;
  if (ret <= 0) {
    if (ret == 0 && (pipe->debug & HNDBG_PIPE)) {
      hin_debug ("pipe %d>%d read EOF\n", pipe->in.fd, pipe->out.fd);
    } else if (ret < 0) {
      hin_error ("pipe %d>%d read error '%s'", pipe->in.fd, pipe->out.fd, strerror (-ret));
      basic_dlist_remove (&pipe->reading, &buffer->pipe_dlist);
      if (pipe->in_error_callback)
        pipe->in_error_callback (pipe, -ret);
    }

    pipe->in.flags |= HIN_DONE;
    buffer->flags |= HIN_DONE;
    ret = 0;
  }

  hin_pipe_check_limits (pipe);

  if ((buffer->flags & HIN_OFFSETS)
   && (ret < buffer->count)
   && ((pipe->in.flags |= HIN_DONE) == 0)) {
    //if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d read incomplete %d/%d\n", pipe->in.fd, pipe->out.fd, ret, buffer->count);

    buffer->ptr += ret;
    buffer->count -= ret;
    buffer->pos += ret;

    if (hin_request_read (buffer) < 0) {
      basic_dlist_remove (&pipe->reading, &buffer->pipe_dlist);
      pipe->num_que--;
      if (pipe->in_error_callback)
        pipe->in_error_callback (pipe, 0);
      hin_pipe_close (pipe);
      return -1;
    }
    return 0;
  }

  if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d read  %d/%d pos %lld left %lld %s\n",
    pipe->in.fd, pipe->out.fd, ret, buffer->count, (long long)buffer->pos, (long long)pipe->in.count, buffer->flags & HIN_DONE ? "flush" : "cont");

  basic_dlist_remove (&pipe->reading, &buffer->pipe_dlist);
  pipe->num_que--;
  buffer->count = ret;
  buffer->ptr = buffer->buffer;
  hin_pipe_process_buffer (pipe, buffer, HIN_PIPE_ALL);

  hin_pipe_advance (pipe);

  return 0;
}


