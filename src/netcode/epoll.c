
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/epoll.h>

#include <basic_hashtable.h>

#include "hin.h"
#include "conf.h"
#include "hin_internal.h"

#define MAX_EVENTS 32
#define HIN_EPOLL_HASH 0x564

typedef struct {
  int fd;
  hin_buffer_t * in_buf, * out_buf;
} hin_epoll_wrapper_t;

static HIN_LOCAL_VAR int epoll_fd = -1;
static HIN_LOCAL_VAR basic_ht_t * epoll_ht = NULL;

static int hin_init_epoll () {
  if (epoll_fd < 0) {
    if (epoll_ht == NULL) {
      epoll_ht = basic_ht_create (1024, HIN_EPOLL_HASH);
    }

    epoll_fd = epoll_create1 (0);
    if (epoll_fd < 0) {
      hin_perror ("epoll_create");
      return -1;
    }
  }
  return 0;
}

void hin_epoll_clean () {
  if (epoll_fd < 0) return ;
  close (epoll_fd);
  epoll_fd = -1;

  if (epoll_ht) {
    basic_ht_iterator_t iter;
    basic_ht_pair_t * pair;
    memset (&iter, 0, sizeof iter);
    while ((pair = basic_ht_iterate_pair (epoll_ht, &iter)) != NULL) {
      hin_epoll_wrapper_t * box = (void*)pair->value1;
      free (box);
    }
    basic_ht_free (epoll_ht);
    epoll_ht = NULL;
  }
}

static int hin_epoll_request (hin_buffer_t * buf, uint32_t events, uint32_t flag) {
  if (hin_init_epoll () < 0) {
    return -1;
  }

  hin_epoll_wrapper_t * box;
  basic_ht_pair_t * pair = basic_ht_get_pair (epoll_ht, buf->fd, buf->fd);
  if (pair) {
    box = (hin_epoll_wrapper_t*)pair->value1;
  } else {
    box = calloc (1, sizeof (*box));
    box->fd = buf->fd;
    basic_ht_set_pair (epoll_ht, buf->fd, buf->fd, (uintptr_t)box, buf->fd);
  }

  if (events == EPOLLIN) {
    if (box->in_buf) hin_error ("already reading %d %x", buf->fd, flag);
    box->in_buf = buf;
    if (box->out_buf) events |= EPOLLOUT;
  } else if (events == EPOLLOUT) {
    if (box->out_buf) hin_error ("already writing %d %x", buf->fd, flag);
    box->out_buf = buf;
    if (box->in_buf) events |= EPOLLIN;
  }

  struct epoll_event event;
  event.events = events | EPOLLONESHOT;
  event.data.ptr = box;

  if ((buf->flags & HIN_EPOLL) == HIN_EPOLL) {
    buf->flags &= (~HIN_EPOLL) | flag;
  }

  int op = EPOLL_CTL_MOD;
  if (pair == NULL) op = EPOLL_CTL_ADD;
  if (epoll_ctl (epoll_fd, op, buf->fd, &event) < 0) {
    if (errno == ENOENT) {
      if (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, buf->fd, &event) >= 0) {
        return 0;
      }
    }
    hin_perror ("epoll_ctl fd %d %x", buf->fd, flag);
    return -1;
  }

  return 0;
}

int hin_epoll_request_accept (hin_buffer_t * buf) {
  return hin_epoll_request (buf, EPOLLIN, HIN_EPOLL_ACCEPT);
}

int hin_epoll_request_read (hin_buffer_t * buf) {
  return hin_epoll_request (buf, EPOLLIN, HIN_EPOLL_READ);
}

int hin_epoll_request_connect (hin_buffer_t * buf) {
  hin_client_t * client = (hin_client_t*)buf->parent;
  client->ai_addrlen = sizeof (client->ai_addr);

  if (hin_make_socket_non_blocking (buf->fd) < 0) {
    return -1;
  }

  int ret = connect (buf->fd, &client->ai_addr, client->ai_addrlen);
  if (ret < 0 && (errno == EINPROGRESS)) { // this is einprogress not eagain
    return hin_epoll_request (buf, EPOLLOUT, HIN_EPOLL_CONNECT);
  } else {
    // has finished immediately
    hin_request_callback (buf, ret);
  }
  return 0;
}

int hin_epoll_request_write (hin_buffer_t * buf) {
  int ret;
  if (buf->flags & HIN_OFFSETS) {
    ret = pwrite (buf->fd, buf->ptr, buf->count, buf->pos);
  } else {
    ret = write (buf->fd, buf->ptr, buf->count);
  }
  if (ret < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
    return hin_epoll_request (buf, EPOLLOUT, HIN_EPOLL_WRITE);
  } else {
    // has finished immediately
    hin_request_callback (buf, ret);
  }
  return 0;
}

static int hin_epoll_buf_exec (hin_buffer_t * buf, hin_epoll_wrapper_t * box) {
  int ret = 0;
  uint32_t flags = buf->flags & HIN_EPOLL;
  //printf ("epoll %d mode %x\n", buf->fd, flags);
  if (flags == HIN_EPOLL_READ) {
    box->in_buf = NULL;
    if (buf->flags & HIN_OFFSETS) {
      ret = pread (buf->fd, buf->ptr, buf->count, buf->pos);
    } else {
      ret = read (buf->fd, buf->ptr, buf->count);
    }
  } else if (flags == HIN_EPOLL_WRITE) {
    box->out_buf = NULL;
    if (buf->flags & HIN_OFFSETS) {
      ret = pwrite (buf->fd, buf->ptr, buf->count, buf->pos);
    } else {
      ret = write (buf->fd, buf->ptr, buf->count);
    }
  } else if (flags == HIN_EPOLL_ACCEPT) {
    hin_client_t * client = (hin_client_t*)buf->parent;
    client->ai_addrlen = sizeof (client->ai_addr);
    ret = accept4 (buf->fd, &client->ai_addr, &client->ai_addrlen, SOCK_NONBLOCK);

    if (ret == -1) {
      if (errno == EAGAIN || errno == EWOULDBLOCK) {
        box->in_buf = buf;
        return 0;
      }
    }
    hin_request_callback_run (buf, ret);
    return 1;
  } else if (flags == HIN_EPOLL_CONNECT) {
    box->out_buf = NULL;
    socklen_t result_len = sizeof (ret);
    if (getsockopt (buf->fd, SOL_SOCKET, SO_ERROR, &ret, &result_len) < 0) {
      hin_perror ("getsockopt");
      ret = -1;
    }
  } else {
    hin_error ("epoll read&write");
    return -1;
  }

  hin_request_callback_run (buf, ret);

  return 0;
}

int hin_epoll_check () {
  if (epoll_fd < 0) return 0;

  int timeout = 0;
  if (hin_g.flags & HIN_FORCE_EPOLL) {
    timeout = HIN_HTTPD_TIME_DT;
  }

  struct epoll_event events[MAX_EVENTS];
  int event_count = epoll_wait (epoll_fd, events, MAX_EVENTS, timeout);
  hin_buffer_t * buf;

  for (int i = 0; i < event_count; i++) {
    struct epoll_event * event = &events[i];
    hin_epoll_wrapper_t * box = event->data.ptr;

    if ((event->events & (EPOLLIN | EPOLLHUP))) {
      while (1) {
        buf = box->in_buf;
        box->in_buf = NULL;
        if (buf == NULL || hin_epoll_buf_exec (buf, box) <= 0) break;
      }
    }
    if ((event->events & (EPOLLOUT | EPOLLHUP))) {
      while (1) {
        buf = box->out_buf;
        box->out_buf = NULL;
        if (buf == NULL || hin_epoll_buf_exec (buf, box) <= 0) break;
      }
    }
  }

  return 0;
}


