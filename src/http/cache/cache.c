
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <fcntl.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/file.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_child.h"

#include <basic_hashtable.h>

typedef struct hin_cache_store_struct {
  int dirfd;
  off_t size, max_size;
  const char * path;
  basic_ht_t ht;
} hin_cache_store_t;

static hin_cache_store_t * default_store = NULL;

struct hin_cache_store_struct * hin_cache_get_default () {
  return default_store;
}

uintptr_t hin_cache_seed (struct hin_cache_store_struct * cache) {
  return cache->ht.seed;
}

// from https://github.com/robertdavidgraham/sockdoc/blob/e1a6fe43389f1033c126e52e8015e30bd26bed6b/src/util-entropy.c
static int hin_random (uint8_t * buf, int sz) {
  int fd;

  do {
    fd = open("/dev/urandom", O_RDONLY, 0);
  } while (fd < 0 && errno == EINTR);

  if (fd < 0) {
    do {
      fd = open("/dev/random", O_RDONLY, 0);
    } while (fd < 0 && errno == EINTR);
  }

  if (fd < 0) {
    hin_error ("can't open /dev/urandom '%s'", strerror (errno));
    return -1;
  }

  /* Read this a byte at a time. This is because in theory,
   * a single read of 64 bytes may result only in a smaller
   * chunk of data. This makes testing when this rarely
   * occurs difficult, so instead just force the case of
   * a byte-at-a-time */
  for (int i = 0; i < sz; i++) {
    int x = read (fd, buf + i, 1);
    if (x != 1) {
      hin_error ("can't read /dev/urandom '%s'", strerror (errno));
      return -1;
    }
  }

  return 0;
}

hin_cache_store_t * hin_cache_create (const char * path, off_t sz) {
  if (default_store) {
    // atm we dont support multiple caches
    return NULL;
  }

  hin_cache_store_t * store = calloc (1, sizeof (hin_cache_store_t));
  uintptr_t seed;
  hin_random ((uint8_t*)&seed, sizeof (seed));
  if (basic_ht_init (&store->ht, 1024, seed) < 0) {
    hin_weird_error (1134557);
  }

  if (default_store == NULL)
    default_store = store;

  store->max_size = sz;
  store->path = strdup (path);

  int slen = strlen (path) + sizeof ("/db") + 1;
  char * new_path = malloc (slen);
  snprintf (new_path, slen, "%s/db", path);
  int fd = hin_open_file_and_create_path (AT_FDCWD, new_path, O_WRONLY | O_APPEND | O_CLOEXEC | O_CREAT, 0660);
  if (fd < 0) {
    hin_error ("openat: %s %s", strerror (errno), new_path);
  }
  close (fd);
  unlinkat (AT_FDCWD, new_path, 0);

  store->dirfd = openat (AT_FDCWD, path, O_DIRECTORY, 0);
  if (store->dirfd < 0) {
    hin_error ("openat: %s %s", strerror (errno), new_path);
  }

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("cache '%s' seed %zx dirfd %d\n", new_path, seed, store->dirfd);

  free (new_path);

  return store;
}

void hin_cache_item_clean (hin_file_t * item) {
  if (item->fd > 0) close (item->fd);
  if (hin_g.debug & HNDBG_CACHE)
    hin_debug ("cache %zx_%zx free\n", item->cache_key1, item->cache_key2);

  hin_timer_remove (&item->timer);

  if (HIN_HTTPD_CACHE_CLEAN_ON_EXIT) {
    hin_cache_store_t * store = item->parent;
    char buffer[70];
    snprintf (buffer, sizeof buffer, "%zx_%zx", item->cache_key1, item->cache_key2);
    if (unlinkat (store->dirfd, buffer, 0) < 0) hin_perror ("unlinkat");
  }

  basic_dlist_t * elem = item->que.next;
  while (elem) {
    hin_file_que_t * que = basic_dlist_ptr (elem, offsetof (hin_file_que_t, list));
    elem = elem->next;

    // TODO flush clients waiting for cache
    basic_dlist_remove (&item->que, &que->list);
    free (que);
  }

  free (item);
}

static void hin_cache_ref (hin_file_t * item) {
  item->refcount++;
}

void hin_cache_unref (hin_file_t * item) {
  item->refcount--;
  if (item->refcount == 0) {
    hin_cache_item_clean (item);
  } else if (item->refcount < 0) {
    hin_error ("cache refcount < 0");
  } else {
    if (hin_g.debug & HNDBG_CACHE)
      hin_debug ("cache %zx_%zx refcount --%d\n", item->cache_key1, item->cache_key2, item->refcount);
  }
}

void hin_cache_remove (hin_cache_store_t * store, hin_file_t * item) {
  basic_ht_delete_pair (&store->ht, item->cache_key1, item->cache_key2);
  store->size -= item->size;
  hin_cache_unref (item);
}

void hin_cache_store_clean (hin_cache_store_t * store) {
  basic_ht_iterator_t iter;
  basic_ht_pair_t * pair;
  memset (&iter, 0, sizeof iter);
  while ((pair = basic_ht_iterate_pair (&store->ht, &iter)) != NULL) {
    hin_file_t * item = (void*)pair->value1;
    hin_cache_unref (item);
  }
  basic_ht_clean (&store->ht);
  close (store->dirfd);
  if (store->path) free ((void*)store->path);
  free (store);
}

void hin_cache_clean () {
  if (default_store == NULL) return ;

  if (hin_g.debug & HNDBG_CACHE)
    hin_debug ("cache clean\n");

  hin_cache_store_clean (default_store);
  default_store = NULL;
}

hin_file_t * hin_cache_get (hin_cache_store_t * store, basic_ht_hash_t key1, basic_ht_hash_t key2) {
  basic_ht_pair_t * pair = basic_ht_get_pair (&store->ht, key1, key2);
  if (pair == NULL) return NULL;
  hin_file_t * item = (void*)pair->value1;
  return item;
}

static int hin_cache_pipe_error_callback (hin_pipe_t * pipe, int err) {
  hin_file_t * item = (hin_file_t*)pipe->parent;
  hin_cache_store_t * store = item->parent;

  hin_error ("cache %zx_%zx error in generating", item->cache_key1, item->cache_key2);

  basic_dlist_t * elem = item->que.next;
  while (elem) {
    hin_file_que_t * que = basic_dlist_ptr (elem, offsetof (hin_file_que_t, list));
    elem = elem->next;

    httpd_client_t * http = que->ptr;
    if (hin_g.debug & HNDBG_CACHE)
      hin_debug (" error to %d\n", http->c.sockfd);
    http->state &= ~HIN_REQ_DATA;
    httpd_error (http, 500, "error! %d", 325346);
    hin_cache_unref (item);

    free (que);
  }

  item->que.next = NULL;
  item->flags |= HIN_CACHE_ERROR;

  basic_ht_delete_pair (&store->ht, item->cache_key1, item->cache_key2);

  return 0;
}

int hin_cache_timeout_callback (hin_timer_t * timer, time_t time) {
  hin_file_t * item = timer->ptr;
  hin_cache_store_t * store = item->parent;
  if (store == NULL) store = default_store;

  if (hin_g.debug & (HNDBG_CACHE|HNDBG_TIMEOUT))
    hin_debug ("cache %zx_%zx timeout for %p\n", item->cache_key1, item->cache_key2, item);
  hin_cache_remove (store, item);
  return 1;
}

int hin_cache_save (void * store1, hin_pipe_t * pipe) {
  hin_cache_store_t * store = store1;
  if (store == NULL) store = default_store;
  httpd_client_t * http = pipe->parent;

  if (http->disable & HIN_HTTP_LOCAL_CACHE) return -1;
  if ((http->cache_key1 | http->cache_key2) == 0) return -1;
  if (store->size >= store->max_size) {
    hin_error ("cache is full");
    return -1;
  }

  hin_file_que_t * que = calloc (1, sizeof (*que));
  que->ptr = http;
  http->cache_flags |= HIN_CACHE_ACTIVE;

  hin_file_t * item = hin_cache_get (store, http->cache_key1, http->cache_key2);
  if (item) {
    // if cache object is present but incomplete register for completion watching and return
    basic_dlist_append (&item->que, &que->list);
    hin_cache_ref (item);
    if (hin_g.debug & HNDBG_CACHE)
      hin_debug ("cache %zx_%zx queue event\n", http->cache_key1, http->cache_key2);
    return 0;
  }

  item = calloc (1, sizeof (*item));
  item->type = HIN_CACHE_OBJECT;
  item->refcount = 2;
  item->cache_key1 = http->cache_key1;
  item->cache_key2 = http->cache_key2;
  item->parent = store;
  basic_dlist_append (&item->que, &que->list);
  basic_ht_set_pair (&store->ht, http->cache_key1, http->cache_key2, (uintptr_t)item, 0);

  if (HIN_HTTPD_CACHE_TMPFILE) {
    item->fd = openat (store->dirfd, ".", O_RDWR | O_TMPFILE, 0600);
    if (item->fd < 0) hin_perror ("openat");
  } else {
    char buffer[70];
    snprintf (buffer, sizeof buffer, "%zx_%zx", http->cache_key1, http->cache_key2);
    item->fd = openat (store->dirfd, buffer, O_RDWR | O_CREAT | O_TRUNC, 0600);
    if (hin_g.debug & HNDBG_CACHE)
      hin_debug ("cache %zx_%zx create fd %d\n", http->cache_key1, http->cache_key2, item->fd);
    if (item->fd < 0) {
      hin_perror ("openat");
      return -1;
    }
  }

  hin_timer_t * timer = &item->timer;
  timer->callback = hin_cache_timeout_callback;
  timer->ptr = item;
  hin_timer_update (timer, time (NULL) + http->cache);

  if (hin_g.debug & (HNDBG_TIMEOUT|HNDBG_CACHE))
    hin_debug ("cache %zx_%zx timeout %p at %lld\n", http->cache_key1, http->cache_key2, timer->ptr, (long long)timer->time);

  pipe->flags |= HIN_HASH | HIN_CACHE;
  pipe->out.fd = item->fd;
  pipe->out.flags = (pipe->out.flags & ~(HIN_SOCKET|HIN_SSL)) | (HIN_FILE|HIN_OFFSETS);
  pipe->parent = item;
  pipe->out_error_callback = hin_cache_pipe_error_callback;
  pipe->in_error_callback = hin_cache_pipe_error_callback;

  // TODO save php headers
  // how do you resend the static resource and notify cache object is done and should be sent
  return 1;
}

int hin_cache_is_public (httpd_client_t * http) {
  if ((http->cache_flags & HIN_CACHE_PUBLIC)
    && http->method != HIN_METHOD_POST
    && http->status == 200) {
    return 1;
  }
  return 0;
}

void hin_cache_serve_client (httpd_client_t * http, hin_file_t * item) {
  http->peer_flags &= ~(HIN_HTTP_CHUNKED);
  http->cache_flags |= HIN_CACHE_ACTIVE;
  http->status = 200;
  hin_timer_t * timer = &item->timer;
  hin_cache_set_number (http, timer->time - time (NULL));
  httpd_send_file (http, item, NULL);
}

int hin_cache_finish (httpd_client_t * http, hin_pipe_t * pipe) {
  hin_file_t * item = (hin_file_t*)pipe->parent;
  if (item->flags & HIN_CACHE_ERROR) {
    hin_cache_unref (item);
    // TODO send errors to clients
    return 0;
  }

  item->size = pipe->out.count;
  item->etag = pipe->hash;
  item->flags |= HIN_CACHE_DONE;
  time (&item->modified);

  if (hin_g.debug & HNDBG_CACHE)
    hin_debug ("cache %zx_%zx ready sz %lld etag %llx\n", item->cache_key1, item->cache_key2, (long long)item->size, (long long)item->etag);

  hin_cache_store_t * store = item->parent;
  store->size += item->size;

  basic_dlist_t * elem = item->que.next;
  while (elem) {
    hin_file_que_t * que = basic_dlist_ptr (elem, offsetof (hin_file_que_t, list));
    elem = elem->next;

    httpd_client_t * http = que->ptr;
    hin_cache_serve_client (http, item);
    basic_dlist_remove (&item->que, &que->list);
    free (que);
  }

  return 0;
}

int hin_cache_check (void * store1, httpd_client_t * http) {
  hin_cache_store_t * store = store1;
  if (store == NULL) store = default_store;

  if (http->disable & HIN_HTTP_LOCAL_CACHE) return -1;
  if ((http->cache_key1 | http->cache_key2) == 0) return -1;

  hin_file_t * item = hin_cache_get (store, http->cache_key1, http->cache_key2);
  if (item == NULL) return 0;

  if (hin_g.debug & HNDBG_CACHE)
    hin_debug ("cache %zx_%zx present\n", http->cache_key1, http->cache_key2);

  hin_cache_ref (item);

  hin_cache_serve_client (http, item);

  return 1;
}


