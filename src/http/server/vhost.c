
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <basic_hashtable.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"

#include "httpd_internal.h"

static httpd_vhost_t * default_vhost = NULL;
static basic_ht_t * default_names = NULL;

#define HIN_DEFAULT_NAME_HASH 101

HIN_EXPORT void httpd_vhost_set_debug (uint32_t debug) {
  hin_g.debug = debug;

  for (basic_dlist_t * elem = hin_g.vhost_list.next; elem; elem = elem->next) {
    httpd_vhost_t * vhost = basic_dlist_ptr (elem, offsetof (httpd_vhost_t, list));
    vhost->debug = debug;
  }

  for (basic_dlist_t * elem = hin_g.server_list.next; elem; elem = elem->next) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));

    server->debug = debug;

    basic_dlist_t * elem1 = server->accept_list.next;
    while (elem1) {
      hin_buffer_t * buf = basic_dlist_ptr (elem1, offsetof (hin_buffer_t, list));
      elem1 = elem1->next;

      buf->debug = debug;
    }

    httpd_vhost_t * vhost = server->c.parent;
    vhost->debug = debug;
  }
}

HIN_EXPORT void hin_ssl_ctx_unref (hin_ssl_ctx_t * box) {
  box->refcount--;
  if (box->refcount > 0) { return ; }
  free ((void*)box->cert);
  free ((void*)box->key);
  // free ssl ctx
  hin_ssl_free_cert (box->ctx);
  free (box);
}

static void httpd_vhost_clean_single (httpd_vhost_t * vhost) {
  hin_ssl_ctx_t * box = vhost->ssl;
  if (box)
    hin_ssl_ctx_unref (box);

  if (vhost->hostname) free (vhost->hostname);
  if (vhost->cwd_fd && vhost->cwd_fd != AT_FDCWD) close (vhost->cwd_fd);
  if (vhost->names) {
    basic_ht_free (vhost->names);
  }
  basic_dlist_remove (&hin_g.vhost_list, &vhost->list);
  free (vhost);
}

HIN_EXPORT void httpd_vhost_clean () {
  basic_dlist_t * elem = hin_g.vhost_list.next;
  while (elem) {
    httpd_vhost_t * vhost = basic_dlist_ptr (elem, offsetof (httpd_vhost_t, list));
    elem = elem->next;

    httpd_vhost_clean_single (vhost);
  }
  if (default_names) {
    basic_ht_free (default_names);
    default_names = NULL;
  }
}

HIN_EXPORT httpd_vhost_t * httpd_vhost_get (httpd_vhost_t * vhost, const char * name, int name_len) {
  basic_ht_t * ht = NULL;
  if (vhost) {
    ht = vhost->names;
  } else {
    ht = default_names;
  }
  if (ht == NULL) {
    if (vhost && vhost->parent)
      return httpd_vhost_get (vhost->parent, name, name_len);
    return NULL;
  }

  basic_ht_hash_t h1 = 0, h2 = 0;
  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  basic_ht_pair_t * pair = basic_ht_get_pair (ht, h1, h2);

  if (pair == NULL && vhost) {
    if (vhost->parent)
      return httpd_vhost_get (vhost->parent, name, name_len);
    return httpd_vhost_get (NULL, name, name_len);
  }
  if (pair == NULL) return NULL;
  return (void*)pair->value2;
}

HIN_EXPORT int httpd_vhost_set_name (httpd_vhost_t * parent, const char * name, int name_len, httpd_vhost_t * new) {
  basic_ht_t * ht = NULL;
  if (parent) {
    if (parent->names == NULL) {
      parent->names = basic_ht_create (1024, HIN_DEFAULT_NAME_HASH);
    }
    ht = parent->names;
  } else {
    if (default_names == NULL) {
      default_names = basic_ht_create (1024, HIN_DEFAULT_NAME_HASH);
    }
    ht = default_names;
  }

  basic_ht_hash_t h1 = 0, h2 = 0;
  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  basic_ht_set_pair (ht, h1, h2, 0, (uintptr_t)new);
  return 1;
}

// create vhost
HIN_EXPORT httpd_vhost_t * httpd_vhost_create (const char * name, httpd_server_t * bind, httpd_vhost_t * parent) {
  if (name == NULL) {
    hin_error ("vhost name missing");
    return NULL;
  }

  httpd_vhost_t * vhost = calloc (1, sizeof (httpd_vhost_t));
  vhost->magic = HIN_VHOST_MAGIC;
  vhost->bind = bind;
  vhost->hostname = strdup (name);

  httpd_vhost_t * patron = NULL;

  if (bind) {
    if (bind->s.c.parent == NULL) {
      bind->s.c.parent = vhost;
      patron = vhost;
    } else {
      patron = bind->s.c.parent;
    }
  }
  if (default_vhost == NULL || (default_vhost->bind == NULL && bind)) {
    default_vhost = vhost;
  }

  httpd_vhost_set_name (patron, name, strlen (name), vhost);

  basic_dlist_append (&hin_g.vhost_list, &vhost->list);

  return vhost;
}

HIN_EXPORT int httpd_vhost_switch (httpd_client_t * http, httpd_vhost_t * vhost) {
  if (vhost == NULL) return -1;
  http->vhost = vhost;
  http->debug = vhost->debug;
  http->disable = vhost->disable;
  httpd_client_ping (http, vhost->timeout);
  return 0;
}

int httpd_vhost_request (httpd_client_t * http, const char * name, int len) {
  if (http->hostname) {
    if ((len == (int)strlen (http->hostname)) && strncmp (http->hostname, name, len) == 0)
      return 0;
    return -1;
  }

  httpd_vhost_t * vhost = httpd_vhost_get (http->vhost, name, len);
  if (http->debug & (HNDBG_HTTP|HNDBG_INFO)) {
    hin_debug ("  hostname '%.*s' vhost %s\n", len, name, vhost ? vhost->hostname : "not found");
  }

  if (vhost == NULL) {
    return -1;
  }

  http->hostname = strndup (name, len);
  httpd_vhost_switch (http, vhost);

  return 0;
}

HIN_EXPORT httpd_vhost_t * httpd_vhost_default () {
  if (default_vhost == NULL) {
    //hin_error ("vhost default missing");
    //exit (1);
  }
  return default_vhost;
}

/*
int httpd_vhost_set_default (hin_server_t * bind, httpd_vhost_t * new) {
  // TODO make set default vhost function
  // needs to move old vhosts to new hashtable
  return 0;
}*/




