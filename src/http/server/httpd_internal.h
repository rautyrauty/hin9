
#ifndef HIN_HTTPD_INTERNAL_H
#define HIN_HTTPD_INTERNAL_H

int httpd_client_reread (httpd_client_t * http);

// filters
int httpd_request_chunked (httpd_client_t * http);

// internal
int hin_http_state (http_client_t * http, int state, uintptr_t data);

int httpd_pipe_set_http11_response_options (httpd_client_t * http, hin_pipe_t * pipe);

int http_connection_start (http_client_t * http);
http_client_t * http_connection_get (const char * url1);
int http_connection_allocate (http_client_t * http);
int http_connection_release (http_client_t * http);

int httpd_process (httpd_client_t * http);

int httpd_headers_parse (httpd_client_t * http, string_t * source);

int hin_client_deflate_init (httpd_client_t * http);

int httpd_pipe_upload_chunked (httpd_client_t * http, hin_pipe_t * pipe);

int httpd_file_is_multirange (httpd_client_t * http);

int httpd_parse_req (httpd_client_t * http, string_t * source);
int httpd_increase_annoyance (httpd_client_t * http, int annoyance);

extern httpd_annoyance_t default_annoy;

#endif

