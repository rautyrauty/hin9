
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#ifndef STATX_MTIME
#include <linux/stat.h>
#endif

#include <basic_pattern.h>
#include <basic_vfs.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/file.h"
#include "http/vhost.h"
#include "conf.h"

#include "httpd_internal.h"

static int httpd_close_filefd (httpd_client_t * http, hin_buffer_t * buf) {
  if (http->file_fd <= 0) return 0;
  if (http->debug & HNDBG_SYSCALL) hin_debug ("close filefd %d\n", http->file_fd);
  close (http->file_fd);
  http->file_fd = 0;
  if (buf) {
    hin_buffer_clean (buf);
  }
  return 0;
}

static int httpd_send_file_done (hin_pipe_t * pipe) {
  if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d file done %lld\n", pipe->in.fd, pipe->out.fd, (long long)pipe->out.count);

  httpd_client_t * http = pipe->parent;

  httpd_client_finish_output (http, pipe);

  return 0;
}

HIN_EXPORT int httpd_send_file (httpd_client_t * http, hin_file_t * file, hin_buffer_t * buf) {
  off_t sz = file->size;

  if (buf == NULL) {
    buf = malloc (sizeof (*buf) + READ_SZ);
    memset (buf, 0, sizeof (*buf));
    buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
    buf->fd = http->c.sockfd;
    buf->count = 0;
    buf->sz = READ_SZ;
    buf->ptr = buf->buffer;
    buf->parent = http;
    buf->ssl = http->c.ssl;
  }

  if (http->debug & HNDBG_PIPE) {
    hin_debug ("pipe %d>%d file '%s' sz %lld\n", file->fd, http->c.sockfd, http->file_path, (long long)sz);
  }

  // do you need to check http->status for 200 or can you return a 304 for a 206
  if ((http->disable & HIN_HTTP_MODIFIED)) {
    http->modified_since = 0;
  }

  if ((http->disable & HIN_HTTP_ETAG)) {
    http->etag = 0;
  }

  if (http->modified_since && http->modified_since < file->modified) {
    http->status = 304;
  }

  if (http->etag) {
    if (http->etag == file->etag) {
      http->status = 304;
    } else {
      http->status = 200;
    }
  }

  if (HIN_HTTPD_MAX_COMPRESS_SIZE && sz > HIN_HTTPD_MAX_COMPRESS_SIZE) {
    http->disable |= HIN_HTTP_COMPRESS;
  }
  http->peer_flags &= ~http->disable;
  http->state |= HIN_REQ_DATA;

  int num_fragments = 0;
  off_t send_sz = 0;
  basic_dlist_t * elem = http->range_requests.next;
  while (elem) {
    httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
    elem = elem->next;

    if (range->start < 0) range->start = sz + range->start;
    if (range->end < 0) range->end = sz;

    if (range->start > sz || range->end > sz || range->start >= range->end) {
      httpd_error (http, 416, "out of range %s %lld-%lld>%lld", http->file_path, range->start, range->end, sz);
      httpd_close_filefd (http, buf);
      return 0;
    }

    if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
      hin_debug ("httpd %d range req %lld-%lld/%lld\n", http->c.sockfd, (long long)range->start, (long long)range->end, (long long)sz);

    num_fragments++;
    send_sz += range->end - range->start;
  }
  if (num_fragments > 0
  && num_fragments == 1
  && http->status == 200
  && (http->disable & HIN_HTTP_RANGE) == 0
  && send_sz < sz) {
    http->status = 206;
  } else {
    send_sz = sz;
  }

  hin_pipe_t * pipe = calloc (1, sizeof (*pipe));
  hin_pipe_init (pipe);
  pipe->in.fd = file->fd;
  pipe->in.flags = HIN_OFFSETS | HIN_FILE | HIN_COUNT;
  pipe->finish_callback = httpd_send_file_done;
  pipe->flags |= HIN_CONDENSE;
  pipe->sz = sz;

  if (file->type) { pipe->parent1 = file; }

  httpd_pipe_set_http11_response_options (http, pipe);

  hin_header (buf, "HTTP/1.%d %d %s\r\n", http->peer_flags & HIN_HTTP_VER10 ? 0 : 1, http->status, http_status_name (http->status));
  httpd_write_common_headers (http, buf);

  if ((http->disable & HIN_HTTP_MODIFIED) == 0 && file->modified) {
    hin_header_date (buf, "Last-Modified: " HIN_HTTP_DATE_FORMAT "\r\n", file->modified);
  }
  if ((http->disable & HIN_HTTP_ETAG) == 0 && file->etag) {
    hin_header (buf, "ETag: \"%llx\"\r\n", file->etag);
  }
  if ((http->disable & HIN_HTTP_RANGE) == 0 && (http->peer_flags & HIN_HTTP_CHUNKED) == 0) {
    hin_header (buf, "Accept-Ranges: bytes\r\n");
  }

  elem = http->range_requests.next;
  httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
  const char * boundary = "3d6b6a416f9b5";
  if ((http->status == 206)) {
    http->current_range = range;
    if (httpd_file_is_multirange (http)) {
      hin_header (buf, "Content-Type: multipart/byteranges; boundary=%s\r\n", boundary);
      // TODO set content length
    } else {
      hin_header (buf, "Content-Range: bytes %lld-%lld/%lld\r\n", range->start, (range->end-1), sz);
    }
    pipe->in.pos = range->start;
    pipe->sz = range->end - range->start;
  }

  if ((http->peer_flags & HIN_HTTP_CHUNKED) == 0) {
    hin_header (buf, "Content-Length: %lld\r\n", send_sz);
  }

  hin_header (buf, "\r\n");

  if (http->debug & HNDBG_RW)
    hin_debug ("httpd %d file response %d '\n%.*s'\n", http->c.sockfd, buf->count, buf->count, buf->ptr);

  hin_pipe_append_raw (pipe, buf);
  hin_pipe_start (pipe);

  return 0;
}

static int httpd_statx_callback (hin_buffer_t * buf, int ret) {
  hin_client_t * client = (hin_client_t*)buf->parent;
  httpd_client_t * http = (httpd_client_t*)client;
  if (ret < 0) {
    httpd_error (http, 404, "can't open '%s': %s", http->file_path, strerror (-ret));
    httpd_close_filefd (http, buf);
    return 1;
  }
  struct statx stat1 = *(struct statx *)buf->ptr;
  struct statx * stat = &stat1;

  uint64_t etag = 0;
  etag += stat->stx_mtime.tv_sec * 0xffff;
  etag += stat->stx_mtime.tv_nsec * 0xff;
  etag += stat->stx_size;

  hin_file_t file;
  memset (&file, 0, sizeof (file));
  file.fd = http->file_fd;
  file.size = stat->stx_size;
  file.etag = etag;
  file.modified = stat->stx_mtime.tv_sec;
  file.type = 0;

  httpd_send_file (http, &file, buf);

  return 0;
}

static int httpd_open_filefd_callback (hin_buffer_t * buf, int ret) {
  httpd_client_t * http = (httpd_client_t*)buf->parent;
  httpd_server_t * server = http->c.parent;
  if (ret < 0) {
    if (server->error_callback) {
      if (server->error_callback (http, 404, "can't open") == 0)
        httpd_error (http, 404, "can't open '%s': %s", http->file_path, strerror (-ret));
    }
    return -1;
  }
  http->file_fd = ret;
  memset (buf->ptr, 0, sizeof (struct statx));

  if (HIN_HTTPD_ASYNC_STATX) {
    buf->flags |= HIN_SYNC;
  }
  buf->callback = httpd_statx_callback;
  hin_request_statx (buf, ret, "", AT_EMPTY_PATH, STATX_ALL);
  return 0;
}

HIN_EXPORT int httpd_handle_file_request (httpd_client_t * http, const char * path, off_t pos, off_t count, uintptr_t param) {
  if (http->state & HIN_REQ_DATA) return -1;
  http->state |= HIN_REQ_DATA;
  http->peer_flags &= ~(HIN_HTTP_CHUNKED);

  if (http->method == HIN_METHOD_POST) {
    httpd_error (http, 405, "post on a file resource");
    return 0;
  }

  if (http->file) {
    basic_vfs_node_t * node = http->file;
    basic_vfs_file_t * f = basic_vfs_get_file (node);
    if (f == NULL) {
      httpd_error (http, 404, "can't open");
      return 0;
    }
    if (node->flags & BASIC_VFS_FORBIDDEN) {
      httpd_error (http, 403, "can't open");
      return 0;
    }

    hin_file_t file;
    memset (&file, 0, sizeof (file));
    file.type = 0;
    file.fd = f->fd;
    file.modified = f->modified;
    file.size = f->size;
    file.etag = f->etag;

    httpd_send_file (http, &file, NULL);
    return 0;
  } else if (path == NULL) {
    httpd_error (http, 404, "no file path given");
    return 0;
  }

  if (http->file_path) free (http->file_path);
  http->file_path = strdup (path);

  // TODO
  if (pos != 0 || count != -1) hin_error ("file offsets were disabled");

  hin_buffer_t * buf = malloc (sizeof (*buf) + READ_SZ);
  memset (buf, 0, sizeof (*buf));
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->count = 0;
  buf->sz = READ_SZ;
  buf->ptr = buf->buffer;
  buf->parent = http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  hin_server_t * socket = http->c.parent;
  httpd_vhost_t * vhost = socket->c.parent;

  if (HIN_HTTPD_ASYNC_OPEN) {
    buf->flags |= HIN_SYNC;
  }
  buf->callback = httpd_open_filefd_callback;
  hin_request_openat (buf, vhost->cwd_fd, path, O_RDONLY | O_CLOEXEC, 0);
  return 0;
}



