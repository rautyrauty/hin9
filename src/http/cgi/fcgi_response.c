
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"

#include <basic_endianness.h>

#include "fcgi.h"

static int hin_fcgi_pipe_write (hin_fcgi_worker_t * worker, FCGI_Header * head) {
  if (worker->http == NULL) {
    // TODO cancel request
    return 0;
  }
  if (worker->header_buf) {
    hin_lines_write (worker->header_buf, (char*)head->data, head->length);
  } else {
    hin_buffer_t * buf = hin_buffer_create_from_data (worker->out, (char*)head->data, head->length);
    httpd_client_t * http = worker->http;
    buf->debug = http->debug;
    hin_pipe_write_process (worker->out, buf, HIN_PIPE_ALL);
    hin_pipe_advance (worker->out);
  }
  return 0;
}

static int hin_fcgi_pipe_end (hin_fcgi_worker_t * worker, FCGI_Header * head) {
  hin_fcgi_socket_t * socket = worker->socket;
  httpd_client_t * http = worker->http;
  FCGI_EndRequestBody * req = (FCGI_EndRequestBody*)head->data;
  req->appStatus = endian_swap32 (req->appStatus);

  hin_pipe_t * pipe = worker->out;
  if (pipe)
    hin_pipe_finish (pipe);

  worker->out = NULL;
  worker->io_state &= ~HIN_REQ_FCGI;
  hin_fcgi_worker_reset (worker);

  if (http == NULL) return 0;

  if (http->debug & HNDBG_CGI)
    hin_debug ("httpd %d fcgi %d worker %d status %d proto %d end\n", http->c.sockfd, socket->fd, head->request_id, req->appStatus, req->protocolStatus);

  if (req->protocolStatus != FCGI_REQUEST_COMPLETE || req->appStatus) {
    hin_error ("httpd %d fcgi %d finish error", http->c.sockfd, socket->fd);
  }

  return 0;
}

int hin_fcgi_read_rec (hin_buffer_t * buf, char * ptr, int left) {
  FCGI_Header * head = (FCGI_Header*)ptr;
  if (left < (int)sizeof (*head)) return 0;
  int len = endian_swap16 (head->length);
  int total = len + sizeof (*head) + head->padding;
  if (left < total) {
    buf->count = total; // hint to set request size

    if (buf->debug & HNDBG_CGI)
      hin_debug ("fcgi %d request more %d<%d\n", buf->fd, left, total);

    return 0;
  }

  head->request_id = endian_swap16 (head->request_id);
  head->length = len;

  hin_fcgi_socket_t * sock = buf->parent;
  int req_id = head->request_id;
  if (req_id < 0 || req_id > sock->max_worker) {
    hin_error ("fcgi %d req_id %d outside bounds", buf->fd, req_id);
    return total;
  }
  hin_fcgi_worker_t * worker = sock->worker[req_id];

  if (buf->debug & HNDBG_CGI)
    hin_debug ("httpd %d fcgi %d worker %d type %d len %d\n", worker->http ? worker->http->c.sockfd : -1, buf->fd, head->request_id, head->type, head->length);

  switch (head->type) {
  case FCGI_STDOUT:
    hin_fcgi_pipe_write (worker, head);
  break;
  case FCGI_STDERR:
    hin_error ("fcgi %d error %d '%.*s'", buf->fd, len, len, head->data);
    // TODO how do you get separate messages from this ?
  break;
  case FCGI_END_REQUEST:
    hin_fcgi_pipe_end (worker, head);
  break;
  default:
    hin_error ("fcgi %d unkown request type %d", buf->fd, head->type);
  }
  return total;
}

int hin_fcgi_socket_read_callback (hin_buffer_t * buf, int ret) {
  hin_fcgi_socket_t * socket = buf->parent;
  if (ret < 0) {
    hin_error ("fcgi %d: '%s'", buf->fd, strerror (-ret));
    hin_fcgi_socket_close (socket);
    return 0;
  }
  if (ret == 0) {
    if (buf->debug & HNDBG_CGI)
      hin_debug ("fcgi %d eof\n", buf->fd);
    hin_fcgi_socket_close (socket);
    return 0;
  }

  if (buf->debug & HNDBG_CGI)
    hin_debug ("fcgi %d received %d bytes\n", buf->fd, ret);

  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;
  char * ptr = lines->base;
  int left = lines->count;
  int sz = 0;
  int used = 0;

  while ((sz = hin_fcgi_read_rec (buf, ptr, left)) > 0) {
    left -= sz;
    ptr += sz;
    used += sz;
  }

  return used;
}


