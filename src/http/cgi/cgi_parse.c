
#if defined (HIN_USE_CGI) || defined (HIN_USE_FCGI)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/uri.h"
#include "http/vhost.h"
#include "conf.h"

#include "fcgi.h"

static int hin_fcgi_pipe_finish_callback (hin_pipe_t * pipe);
static int hin_pipe_cgi_server_finish_callback (hin_pipe_t * pipe);

static int hin_cgi_headers_read_callback (hin_buffer_t * buffer, int received) {
  hin_fcgi_worker_t * worker = (hin_fcgi_worker_t *)buffer->parent;
  httpd_client_t * http = worker->http;
  string_t source1, * source = &source1;
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  source->ptr = lines->base;
  source->len = lines->count;

  string_t line, orig=*source, param1;
  http->status = 200;
  off_t sz = 0;
  uint32_t disable = 0;
  while (1) {
    if (hin_find_line (source, &line) == 0) { return 0; }
    if (line.len == 0) break;
    if (matchi_string (&line, "Status: (%d+)", &param1) > 0) {
      http->status = atoi (param1.ptr);

      if (http->debug & HNDBG_CGI)
        hin_debug ("cgi %d status is %d\n", http->c.sockfd, http->status);

    } else if (matchi_string_equal (&line, "Content-Length: (%d+)", &param1) > 0) {
      sz = atoi (param1.ptr);
      http->count = sz;
    } else if (matchi_string (&line, "Content-Encoding:") > 0) {
      disable |= HIN_HTTP_COMPRESS;
    } else if (matchi_string (&line, "Transfer-Encoding:") > 0) {
      disable |= HIN_HTTP_CHUNKED;
    } else if (matchi_string (&line, "Cache-Control:") > 0) {
      disable |= HIN_HTTP_CACHE;
      httpd_parse_cache_str (line.ptr, line.len, &http->cache_flags, &http->cache);
    } else if (matchi_string (&line, "Date:") > 0) {
      disable |= HIN_HTTP_DATE;
    }
  }

  int len = source->len;
  if (sz && sz < len) len = sz;

  hin_pipe_t * pipe = calloc (1, sizeof (*pipe));
  hin_pipe_init (pipe);
  pipe->in.fd = buffer->fd;
  pipe->in.flags = 0;
  pipe->in.pos = 0;
  pipe->parent = http;
  pipe->parent1 = worker;

  if (worker->socket) {
    pipe->in.flags = HIN_INACTIVE;
    pipe->finish_callback = hin_fcgi_pipe_finish_callback;
  } else {
    pipe->finish_callback = hin_pipe_cgi_server_finish_callback;
  }
  worker->out = pipe;
  worker->header_buf = NULL;

  http->disable |= disable;
  http->peer_flags = http->peer_flags & (~http->disable);

  if (hin_cache_is_public (http)) {
    // cache check is somewhere else
    int n = hin_cache_save (NULL, pipe);
    if (n == 0) {
      // pipe already started ?
      free (pipe);
      return -1;
    } else if (n > 0) {
      if (len > 0) {
        hin_buffer_t * buf1 = hin_buffer_create_from_data (pipe, source->ptr, len);
        hin_pipe_write_process (pipe, buf1, HIN_PIPE_ALL);
      }
      hin_pipe_start (pipe);
      return -1;
    }
  }

  httpd_pipe_set_http11_response_options (http, pipe);

  int sz1 = source->len + READ_SZ;
  hin_buffer_t * buf = malloc (sizeof (*buf) + sz1);
  memset (buf, 0, sizeof (*buf));
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->count = 0;
  buf->sz = sz1;
  buf->ptr = buf->buffer;
  buf->parent = pipe;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  *source = orig;
  hin_header (buf, "HTTP/1.%d %d %s\r\n", http->peer_flags & HIN_HTTP_VER10 ? 0 : 1, http->status, http_status_name (http->status));

  httpd_write_common_headers (http, buf);

  if (http->debug & (HNDBG_CGI|HNDBG_RW))
    fprintf (stderr, "httpd %d cgi read\n", http->c.sockfd);

  while (1) {
    if (hin_find_line (source, &line) == 0) { hin_buffer_clean (buf); return 0; }
    if (line.len == 0) break;

    if (http->debug & (HNDBG_CGI|HNDBG_RW))
      fprintf (stderr, " %zd '%.*s'\n", line.len, (int)line.len, line.ptr);

    if (matchi_string (&line, "Status:") > 0) {
    } else if ((http->peer_flags & HIN_HTTP_CHUNKED) && matchi_string (&line, "Content-Length:") > 0) {
    } else if (HIN_HTTPD_DISABLE_POWERED_BY && matchi_string (&line, "X-Powered-By:") > 0) {
    } else if (matchi_string (&line, "Location:") > 0) {
      hin_cgi_location (http, buf, &line);
    } else if (match_string (&line, "X-CGI-") > 0) {
    } else {
      hin_header (buf, "%.*s\r\n", line.len, line.ptr);
    }
  }
  hin_header (buf, "\r\n");

  if (http->debug & HNDBG_RW) {
    hin_debug ("httpd %d cgi response %d '\n%.*s'\n", http->c.sockfd, buf->count, buf->count, buf->ptr);
    for (basic_dlist_t * elem = buf->list.next; elem; elem=elem->next) {
      hin_buffer_t * buf = hin_buffer_list_ptr (elem);
      hin_debug (" cont %d '\n%.*s'\n", buf->count, buf->count, buf->ptr);
    }
    hin_debug (" left after is %d\n", len);
  }

  hin_pipe_append_raw (pipe, buf);

  if (len > 0) {
    hin_buffer_t * buf1 = hin_buffer_create_from_data (pipe, source->ptr, len);
    hin_pipe_write_process (pipe, buf1, HIN_PIPE_ALL);
  }

  hin_pipe_start (pipe);

  return -1;
}

static int hin_cgi_headers_close_callback (hin_buffer_t * buffer, int ret) {
  hin_fcgi_worker_t * worker = buffer->parent;
  httpd_client_t * http = worker->http;
  httpd_error (http, 500, "cgi process failed %s", strerror (-ret));
  hin_cgi_worker_free (worker);
  return 1;
}

static int hin_cgi_headers_eat_callback (hin_buffer_t * buffer, int num) {
  if (num == 0) {
    hin_lines_request (buffer, 0);
    return 0;
  }
  return 1;
}

#ifdef HIN_USE_CGI

static int hin_pipe_cgi_server_finish_callback (hin_pipe_t * pipe) {
  hin_fcgi_worker_t * worker = (hin_fcgi_worker_t *)pipe->parent1;
  if (pipe->debug & (HNDBG_CGI|HNDBG_PIPE))
    hin_debug ("pipe %d>%d cgi transfer finished bytes %lld\n", pipe->in.fd, pipe->out.fd, (long long)pipe->out.count);

  if (pipe->debug & (HNDBG_CGI|HNDBG_SYSCALL))
    hin_debug ("  cgi read done, close %d\n", pipe->in.fd);
  close (pipe->in.fd);

  free (worker);

  return httpd_client_finish_output (pipe->parent, pipe);
}

int hin_cgi_old_worker_init_pipe (hin_fcgi_worker_t * worker, int fd) {
  httpd_client_t * http = worker->http;

  hin_buffer_t * buf = hin_lines_create_raw (READ_SZ);
  buf->fd = fd;
  buf->parent = worker;
  buf->flags = 0;
  buf->debug = http->debug;

  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;
  lines->read_callback = hin_cgi_headers_read_callback;
  lines->eat_callback = hin_cgi_headers_eat_callback;
  lines->close_callback = hin_cgi_headers_close_callback;

  if (hin_request_read (buf) < 0) {
    httpd_error (http, 503, "error! %d", 435776);
    return -1;
  }

  return 0;
}

#else
static int hin_pipe_cgi_server_finish_callback (hin_pipe_t * pipe) {return -1;}
#endif

#ifdef HIN_USE_FCGI

static int hin_fcgi_pipe_finish_callback (hin_pipe_t * pipe) {
  hin_fcgi_worker_t * worker = pipe->parent1;
  hin_fcgi_socket_t * socket = worker->socket;
  httpd_client_t * http = worker->http;

  if (http && http->debug & HNDBG_CGI)
    hin_debug ("httpd %d fcgi %d worker %d done.\n", http->c.sockfd, socket->fd, worker->req_id);

  http->state &= ~HIN_REQ_FCGI;

  worker->http = NULL;
  worker->io_state &= ~HIN_REQ_DATA;
  hin_fcgi_worker_reset (worker);
  worker->out = NULL;

  return httpd_client_finish_output (http, pipe);
}

int hin_cgi_fast_worker_init_pipe (hin_fcgi_worker_t * worker) {
  httpd_client_t * http = worker->http;

  hin_buffer_t * buf = hin_lines_create_raw (READ_SZ);
  buf->fd = -1;
  buf->parent = worker;
  buf->flags = 0;
  buf->debug = http->debug;

  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;
  lines->read_callback = hin_cgi_headers_read_callback;
  lines->eat_callback = hin_cgi_headers_eat_callback;
  lines->close_callback = hin_cgi_headers_close_callback;

  worker->header_buf = buf;

  return 0;
}

#else
static int hin_fcgi_pipe_finish_callback (hin_pipe_t * pipe) {return -1;}
#endif

#endif

