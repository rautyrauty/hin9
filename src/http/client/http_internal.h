#ifndef HIN_HTTP_INTERNAL_H
#define HIN_HTTP_INTERNAL_H

int http_client_unlink (http_client_t * http);

int http_client_start_headers (http_client_t * http, hin_buffer_t *);

#endif

