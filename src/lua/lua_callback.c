
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <basic_vfs.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/file.h"
#include "http/vhost.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_lua.h"

int hin_set_path (httpd_client_t * http);

int hin_app_process_callback (httpd_client_t * http) {
  httpd_vhost_t * vhost = http->vhost;

  hin_server_t * socket = http->c.parent;
  if (vhost->hsts &&
     (vhost->vhost_flags & HIN_HSTS_NO_REDIRECT) == 0 &&
     socket->ssl_ctx == NULL) {
    httpd_respond_redirect_https (http);
    return 0;
  }

  // prefile callback
  int ret;
  ret = httpd_vhost_map_callback (http, HIN_VHOST_MAP_PREFILE);
  if (ret <= 0) {
    return ret;
  }

  // bind default file
  if (http->file == NULL) {
    hin_set_path (http);
    // if is file then serve file
    // if is directory then try to get index files from lua
    // if no lua try index.html
  }

  // response callback
  ret = httpd_vhost_map_callback (http, HIN_VHOST_MAP_PROCESS);
  if (ret <= 0) {
    return ret;
  }

  // after response callback
  // if file bind (and no response) then send file
  if ((http->state & HIN_REQ_DATA) == 0) {
    httpd_handle_file_request (http, NULL, 0, 0, 0);
  }

  return 0;
}

int hin_app_error_callback (hin_client_t * client, int error_code, const char * msg) {
  httpd_client_t * http = (httpd_client_t*)client;

  if (http->state & HIN_REQ_ERROR_HANDLED) return 0;
  http->state |= HIN_REQ_ERROR_HANDLED;
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_ERROR);

  if (http->state & HIN_REQ_DATA) return 1;
  return 0;
}

int hin_app_finish_output_callback (httpd_client_t * http, hin_pipe_t * pipe) {
  if (pipe->flags & HIN_CACHE) {
    return hin_cache_finish (pipe->parent, pipe);
  }

  int ret = httpd_vhost_map_callback (http, HIN_VHOST_MAP_FINISH);

  if (http->file) {
    basic_vfs_unref (http->file);
  }

  if (http->cache_flags & HIN_CACHE_ACTIVE)
    hin_cache_unref (pipe->parent1);

  if (ret <= 0) {
    return ret;
  }

  return 0;
}


