#ifndef HIN_HTTP_CACHE_H
#define HIN_HTTP_CACHE_H

#include "http/file.h"

enum {
HIN_CACHE_DONE = 0x1, HIN_CACHE_ERROR = 0x2,
HIN_CACHE_PUBLIC = 0x4, HIN_CACHE_PRIVATE = 0x8,
HIN_CACHE_NO_CACHE = 0x10, HIN_CACHE_NO_STORE = 0x20,
HIN_CACHE_NO_TRANSFORM = 0x40, HIN_CACHE_IMMUTABLE = 0x80,
HIN_CACHE_MUST_REVALIDATE = 0x100, HIN_CACHE_PROXY_REVALIDATE = 0x200,
HIN_CACHE_MAX_AGE = 0x400, HIN_CACHE_ACTIVE = 0x800,
};

int hin_cache_save (void * store, hin_pipe_t * pipe);
int hin_cache_finish (httpd_client_t * client, hin_pipe_t * pipe);
int hin_cache_check (void * store, httpd_client_t * client);
int hin_cache_is_public (httpd_client_t * http);

void hin_cache_set_number (httpd_client_t * http, time_t num);
int httpd_parse_cache_str (const char * str, size_t len, uint32_t * flags_out, time_t * max_age);

void hin_cache_unref (hin_file_t * item);

struct hin_cache_store_struct * hin_cache_create (const char * path, off_t sz);
struct hin_cache_store_struct * hin_cache_get_default ();
uintptr_t hin_cache_seed (struct hin_cache_store_struct * cache);

#endif

