#ifndef HTTP_FILE_H
#define HTTP_FILE_H

#include <stdint.h>
#include <time.h>

#include <basic_lists.h>
#include <basic_hashtable.h>

typedef struct hin_file_que_struct {
  void * ptr;
  basic_dlist_t list;
} hin_file_que_t;

typedef struct hin_file_struct {
  int type;
  int fd;
  uint32_t flags;
  uint32_t magic;
  void * parent;

  int refcount;

  basic_ht_hash_t cache_key1, cache_key2;
  hin_timer_t timer;

  time_t modified;
  off_t size;
  uint64_t etag;

  basic_dlist_t que;
} hin_file_t;

int httpd_send_file (httpd_client_t * http, hin_file_t * file, hin_buffer_t * buf);

#endif

