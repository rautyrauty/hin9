
#ifndef HIN_HTTPD_APP_INTERNAL_H
#define HIN_HTTPD_APP_INTERNAL_H

typedef struct {
  const char * exe_path;
  const char * conf_path;
  const char * workdir_path;
  const char * logdir_path;
  const char * tmpdir_path;
  const char * pid_path;
  const char * username;
  const char * groupname;
  const char ** argv;
  const char ** envp;
  int restart_pipe_fd;

  basic_dlist_t cert_list;
} hin_app_t;

extern hin_app_t hin_app;

void hin_app_init ();
void hin_app_clean ();
int hin_app_restart ();
void hin_app_stop ();

int hin_app_is_overloaded ();
void hin_app_restart_finished ();

int hin_console_init ();
void hin_console_clean ();
int hin_signal_init ();
int hin_signal_clean ();
int hin_pidfile (const char * path);
int hin_pidfile_clean ();

int hin_log_flush ();
void hin_lua_clean ();
void hin_cache_clean ();
int hin_vfs_clean ();
void httpd_vhost_clean ();
void hin_fcgi_clean ();

void hin_timer_flush ();
void http_connection_close_idle ();
void httpd_connection_close_idle ();

char * hin_directory_path (const char * old, const char ** replace);
int hin_redirect_log (const char * path);

#include "http/vhost.h"

int hin_cgi (httpd_client_t * http, const char * exe_path, const char * root_path, const char * script_path, const char * path_info);
int hin_fastcgi (httpd_client_t * http, void * fcgi_group, const char * script_path, const char * path_info);
http_client_t * hin_proxy (httpd_client_t * parent, http_client_t * http, const char * url1);

int hin_app_finish_output_callback (struct httpd_client_struct * http, hin_pipe_t * pipe);
int hin_app_process_callback (struct httpd_client_struct * http);
int hin_app_error_callback (hin_client_t * client, int error_code, const char * msg);

struct hin_fcgi_group_struct * hin_fcgi_start (const char * uri, int min, int max);

int httpd_vhost_map_callback (httpd_client_t * http, int type);

int hin_bind_default_vhost (httpd_server_t * sock);

int hin_drop_user (const char * username, const char * groupname);

#endif

